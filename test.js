let excelHandler = require("./excelHandler");

let nameFile = "PORT_IN_AVEN_CO300719.xlsx";

let nameSheet = "Inventario";

let columns = [
  { header: "Número", key: "numero" },
  { header: "Caracter", key: "caracter" },
  { header: "Another", key: "another" }
];

var rows = [{ numero: 1, caracter: "holi", another: 34 }];

//Primero creo el archivo, es decir, primero ejecuto el create, luego el read

//excelHandler.createExcel(nameFile, nameSheet, columns, rows);

excelHandler.readExcel(nameFile);
