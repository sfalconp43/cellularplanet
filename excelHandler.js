"use strict";
 
var Excel = require('exceljs');


    let excelHandler = {
        createExcel: (nameFile, nameSheet, columns, rows) => {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet(nameSheet);
            worksheet.columns = columns;
            worksheet.addRows(rows);
    
            workbook.xlsx.writeFile(nameFile).then(() => {
                console.log(`Archivo '${nameFile}' generado con exito!`)
            });
        },
    
        readExcel: (nameFile, nameColumn) => {
            //return new Promise(function (resolve, reject) {
                var workbook = new Excel.Workbook();
                var data = [];
                var columnData = [];
                workbook.xlsx.readFile(nameFile).then(() => {
                console.log('se leyo el archivo de nombre', nameFile);
                   workbook.eachSheet((sheet) => {
                      // console.log(sheet);
                    var result = sheet.getColumn(nameColumn);
                    result.values.forEach(element => {
                        columnData.push(element)
                        console.log(element);

                    });                  
                        //console.log(result);
                    
                    });                    

                }).catch((error) => {
                });
            //})
        },
        
    }
    
    


module.exports = excelHandler;
