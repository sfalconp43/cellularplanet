'use strict'

const Penalty = require('../models/penalty');
const ICCAssignment = require('../models/ICCAssignment');
// const bcrypt = require('bcrypt');
const config = require('../config');
const fs = require("fs");
var Excel = require('exceljs');


var controller = {

    test: (request, response) => {
        return response.status(200).send({
            message: 'excel controller test'
        });
    },

    activosVerification: async (request, response) => {
        console.log('LLEGO');
        const parameters = request.body;
        let filter = [];
        let update = {ICCStatus: 'Activo'}

        if (request.files.file) {
            var file = request.files.file;
            var path = file.path;
            var fileSplit = path.split('/');
            // var fileSplit = path.split('\\');
            var fileName = fileSplit[2];
        } else {
            return response.status(500).send({ message: "The file is required" });
        }

        fs.rename(`./public/uploads/${fileName}`, './public/uploads/activos.xlsx', err => {
            if (err) {
                console.error(err)
                return
            }
            //done
            var workbook = new Excel.Workbook();
            let ICCList = [];

            console.log('Ates de la lectura');
            workbook.xlsx.readFile('./public/uploads/activos.xlsx').then(() => {
                workbook.eachSheet((sheet) => {
                    var result = sheet.getColumn(parameters.nameColumn);

                    console.log('este es el result', sheet);
    
                    result.values.forEach((element) => {
                        ICCList.push(element);
                    });
    
                    ICCList.shift();
                    console.log(ICCList);
                    ICCList.forEach(element => {
                        filter.push({ ICC: element })
                    });
                    console.log(filter);
                    ICCAssignment.findOneAndUpdate({ $or: filter}, update, { new: true }).then( (result) => {
                        console.log(result);

                        request.flash('success-message', 'Verificacion exitosa');
                        return response.status(200).redirect('verificacion-por-archivo');
                        // return response.status(200).send({
                        //     status: true,
                        //     message: 'successful verification',
                        //     verification: result
                        // });
                    })
                });
            }).catch((error) => {
                console.log(error)
            });

        });

    },

    
    portadosVerification: async (request, response) => {
        const parameters = request.body;
        let filter = [];
        let update = {ICCStatus: 'Portado'}

        if (request.files.file) {
            var file = request.files.file;
            var path = file.path;
            var fileSplit = path.split('/');
            // var fileSplit = path.split('\\');
            var fileName = fileSplit[2];
        } else {
            return response.status(500).send({ message: "The file is required" });
        }

        fs.rename(`./public/uploads/${fileName}`, './public/uploads/portados.xlsx', err => {
            if (err) {
                console.error(err)
                return
            }
            //done
            var workbook = new Excel.Workbook();
            let ICCList = [];

            console.log('Antes de la lectura');
            workbook.xlsx.readFile('./public/uploads/portados.xlsx').then(() => {
                workbook.eachSheet((sheet) => {
                    var result = sheet.getColumn(parameters.nameColumn);
    
                    result.values.forEach((element) => {
                        ICCList.push(element);
                    });
    
                    ICCList.shift();
                    console.log(ICCList);
                    ICCList.forEach(element => {
                        filter.push({ ICC: element })
                    });
                    console.log(filter);
                    ICCAssignment.findOneAndUpdate({ $or: filter}, update, { new: true }).then( (result) => {
                        console.log(result);

                        request.flash('success-message', 'Verificacion exitosa');
                        return response.status(200).redirect('verificacion-por-archivo');

                        // return response.status(200).send({
                        //     status: true,
                        //     message: 'successful verification',
                        //     verification: result
                        // });
                    })
                });
            }).catch((error) => {
                console.log(error)
            });

        });

    },
};

module.exports = controller;