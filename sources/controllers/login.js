const WholeSaler = require('../models/wholeSaler');
const SecondaryWholeSaler = require('../models/secondaryWholeSaler');
const Distributor = require('../models/distributor');
const sellerUser = require('../models/sellerUser');
const bcrypt = require('bcrypt');

var controller = {
    test: (request, response) => {
        return response.status(200).send({
            message: 'login controller test'
        });
    },

    login: async (request, response) => {
        const parameters = request.body;
        const wholeSaler = await WholeSaler.findOne({ email: parameters.email });

        if (!wholeSaler) {
            const secondaryWholeSaler = await SecondaryWholeSaler.findOne({ email: parameters.email });

            if (!secondaryWholeSaler) {
                const distributor = await Distributor.findOne({ email: parameters.email });

                if (!distributor) {
                    const seller = await sellerUser.findOne({ email: parameters.email });

                    if (!seller) {

                        return response.status(200).send({
                            status: false,
                            message: 'Correo electrónico no registrado',
                        });
                    } else {
                        console.log('Es un vendedor');
                        console.log(seller);
                        let user = seller.toObject();
                        delete user.password;
                        user.userType = 'Vendedor';
                        const verifiedPassword = await bcrypt.compare(parameters.password, seller.password);

                        if (verifiedPassword) {
                            return response.status(200).send({
                                status: true,
                                message: 'Login exitoso!',
                                user: user
                            });
                        } else {
                            return response.status(200).send({
                                status: false,
                                message: 'Contraseña Incorrecta',
                            });
                        }
                    }
                } else {
                    console.log('Es un distribuidor');
                    console.log(distributor);
                    let user = distributor.toObject();
                    delete user.password;
                    user.userType = 'Distribuidor';
                    const verifiedPassword = await bcrypt.compare(parameters.password, distributor.password);

                    if (verifiedPassword) {
                        return response.status(200).send({
                            status: true,
                            message: 'Login exitoso!',
                            user: user
                        });
                    } else {
                        return response.status(200).send({
                            status: false,
                            message: 'Contraseña Incorrecta',
                        });
                    }
                }

            } else {
                console.log('Es un mayorista secundario');
                console.log(secondaryWholeSaler);
                let user = secondaryWholeSaler.toObject();
                delete user.password;
                user.userType = 'Mayorista secundario (sucursal)';
                const verifiedPassword = await bcrypt.compare(parameters.password, secondaryWholeSaler.password);

                if (verifiedPassword) {
                    return response.status(200).send({
                        status: true,
                        message: 'Login exitoso!',
                        user: user
                    });
                } else {
                    return response.status(200).send({
                        status: false,
                        message: 'Contraseña Incorrecta',
                    });
                }
            }
        } else {
            console.log('Es un mayorista');
            console.log(wholeSaler);
            let user = wholeSaler.toObject();
            delete user.password;
            user.userType = 'Mayorista principal';
            const verifiedPassword = await bcrypt.compare(parameters.password, wholeSaler.password);

            if (verifiedPassword) {
                return response.status(200).send({
                    status: true,
                    message: 'Login exitoso!',
                    user: user
                });
            } else {
                return response.status(200).send({
                    status: false,
                    message: 'Contraseña Incorrecta',
                });
            }
        }
    },

    logout: (request, response) => {
        request.logout();
        response.redirect('/login');
    }
};

module.exports = controller;