"use strict";

const config = require("../config");
let excelHandler = require("./excelHandler");
const Assignment = require("../models/assignment");
const express = require("express");
const fs = require("fs");


var controller = {
  test: (request, response) => {
    console.log(request.body);
    return response.status(200).send({
      message: "Whole Saler controller test"
    });
  },

  readActive: async (request, response) => {
    console.log("este es el controller");
    const parameters = request.body;


    if (request.files.file) {
        var file = request.files.file;

        var path = file.path;
      
        var newName = file.name;
      
        var fileSplit = path.split('/');
        var fileName = fileSplit[2];
      
    } else {
        return response.status(500).send({ message: "The file is required" });
    }

    fs.rename(`./public/uploads/${fileName}`, `./public/uploads/${newName}`, err => {
        if (err) {
          console.error(err)
          return
        }
        //done
      })
    


    // let nameFile = "public/uploads/testing.xlsx"; //pasar path del metodo de upload
    let nameFile = `./public/uploads/${newName}`;
    
    var typeFile = request.body.type;

    let nameColumn = request.body.column;

    console.log(nameColumn);

    excelHandler.readExcel(nameFile, nameColumn, typeFile);
  },

  readActiveWeb: async (request, response) => {
    console.log("este es el controller");
    const parameters = request.body;

    console.log(request)
    if (request.files.file) {
        var file = request.files.file;

        var path = file.path;
      
        var newName = file.name;
      
        var fileSplit = path.split('/');
        var fileName = fileSplit[2];
      
    } else {
        return response.status(500).send({ message: "The file is required" });
    }

    fs.rename(`./public/uploads/${fileName}`, `./public/uploads/${newName}`, err => {
        if (err) {
          console.error(err)
          return
        }
        //done
      })
    
    // let nameFile = "public/uploads/testing.xlsx"; //pasar path del metodo de upload
    let nameFile = `./public/uploads/${newName}`;
    
    var typeFile = request.body.type;

    let nameColumn = request.body.column;

    console.log(nameColumn);
    console.log(typeFile);

    excelHandler.readExcel2(nameFile, nameColumn, typeFile).then( () => {
      return response.status(200).redirect('verificacion-por-archivo');
      console.log('exito')
    }).catch(()=>{
      console.log('error')
      return response.status(200).redirect('verificacion-por-archivo');
    });
  }
};

module.exports = controller;
