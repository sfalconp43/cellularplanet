'use strict'

const Penalty = require('../models/penalty');
const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        return response.status(200).send({
            message: 'penalty controller test'
        });
    },

    createPenalty: async (request, response) => {
        const parameters = request.body
        var penalty = new Penalty();


        if (parameters.distributor) {
            penalty.distributor = parameters.distributor;
            penalty.penaltyPoints = 2* config.ICC_POINT;
        } else {
            return response.status(200).send({
                status: false,
                message: 'distributor cannot be empty',
            });
        }

        const penaltyStored = await penalty.save();

        return response.status(200).send({
            status: true,
            message: 'penalty created',
            penaltyStored: penaltyStored
        });
    },
};

module.exports = controller;