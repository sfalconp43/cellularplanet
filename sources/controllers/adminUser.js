'use strict'

const AdminUser = require('../models/adminUser');
const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Admin user controller test'
        });
    },

    saveAdminUser: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var adminUser = new AdminUser();

        adminUser.name = parameters.name;
        adminUser.lastname = parameters.lastname;
        adminUser.email = parameters.email;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        adminUser.password = passwordEscripted;
        const adminUserAux = await AdminUser.findOne({ email: adminUser.email });

        if (!adminUserAux) {
            const adminUserStored = await adminUser.save();

            return response.status(200).send({
                status: true,
                message: 'Registered user successfully',
                adminUserStored: adminUserStored
            });

        } else {
            return response.status(200).send({
                status: false,
                message: 'User already registered',
            });
        }
    },

    getAdminUser: async (request, response) => {
        const parameters = request.body
        var adminUserId = parameters.id;

        const adminUser = await AdminUser.findById(adminUserId);

        if (!adminUser) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
            });
        } else {
            let adminUserDB = adminUser.toObject();
            delete adminUserDB.password;

            return response.status(200).send({
                status: true,
                message: 'User found',
                adminUser: adminUserDB
            });
        }
    },

    getAdminUsers: async (request, response) => {
        const adminUsers = await AdminUser.find({});

        let adminUsersDB = [];

        adminUsers.forEach(adminUser => {
            let adminUserDB = adminUser.toObject();
            delete adminUserDB.password;
            adminUsersDB.push(adminUserDB);
        });

        return response.status(200).send({
            status: true,
            message: 'list of administrators',
            adminUsers: adminUsersDB
        });
    },

    updateAdminUser: async (request, response) => {
        let update = {};
        const parameters = request.body
        const adminUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const adminUserUpdate = await AdminUser.findByIdAndUpdate(adminUserId, update, { new: true });

        if (!adminUserUpdate) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                adminUserUpdate: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'Updated user',
                adminUserUpdate: adminUserUpdate
            });
        }
    },

    deleteAdminUser: async (request, response) => {
        const parameters = request.body;
        const adminUserId = parameters.id;

        const adminUserDelete = await AdminUser.findByIdAndRemove(adminUserId);

        if (!adminUserDelete) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                adminUserDelete: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'deleted user',
                adminUserDelete: adminUserDelete
            });
        }
    },
};

module.exports = controller;