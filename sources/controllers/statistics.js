const ICCAssignment = require('../models/ICCAssignment');
const bcrypt = require('bcrypt');

var controller = {
    test: (request, response) => {
        return response.status(200).send({
            message: 'statistics controller test'
        });
    },

    getStatistics: async (request, response) => {
        const parameters = request.query;

        if (parameters.id) {

        } else {
            return response.status(200).send({
                status: false,
                message: 'id cannot be empty',
            });
        }


        if (parameters.userType) {
            switch (parameters.userType) {
                case 'Mayorista principal':
                    // Todo
                    const assignmentsMPAll = await ICCAssignment.find();
                    let inactivosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMPAllP = 0;
                    let activosMPAllP = 0;
                    let portadosMPAllP = 0;

                    if (assignmentsMPAll.length > 0) {
                        if (inactivosMPAll.length > 0) {
                            inactivosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMPAll.length;
                        }

                        if (activosMPAll.length > 0) {
                            activosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMPAll.length;
                        }

                        if (portadosMPAll.length > 0) {
                            portadosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMPAll.length;
                        }
                    }

                    // Ultimo mes
                    const assignmentsMPOne = await ICCAssignment.find({ $or: [{ secondaryWholesalerDate: { $gte: new Date(Date.now() - 2592000000), $lte: new Date() } }, { distributorDate: { $gte: new Date(Date.now() - 2592000000), $lte: new Date() } }] });
                    let inactivosMPOne = assignmentsMPOne.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMPOne = assignmentsMPOne.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMPOne = assignmentsMPOne.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMPOneP = 0;
                    let activosMPOneP = 0;
                    let portadosMPOneP = 0;

                    console.log('Ultimo mes');
                    console.log(assignmentsMPOne);

                    if (assignmentsMPOne.length > 0) {
                        if (inactivosMPOne.length > 0) {
                            inactivosMPOneP = assignmentsMPOne.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMPOne.length;
                        }

                        if (activosMPOne.length > 0) {
                            activosMPOneP = assignmentsMPOne.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMPOne.length;
                        }

                        if (portadosMPOne.length > 0) {
                            portadosMPOneP = assignmentsMPOne.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMPOne.length;
                        }
                    }

                    // Ultimos tres meses
                    const assignmentsMPThree = await ICCAssignment.find({ $or: [{ secondaryWholesalerDate: { $gte: new Date(Date.now() - 7776000000), $lte: new Date() } }, { distributorDate: { $gte: new Date(Date.now() - 7776000000), $lte: new Date() } }] });
                    let inactivosMPThree = assignmentsMPThree.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMPThree = assignmentsMPThree.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMPThree = assignmentsMPThree.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMPThreeP = 0;
                    let activosMPThreeP = 0;
                    let portadosMPThreeP = 0;

                    console.log('Ultimos tres meses');
                    console.log(assignmentsMPThree);

                    if (assignmentsMPThree.length > 0) {
                        if (inactivosMPThree.length > 0) {
                            inactivosMPThreeP = assignmentsMPThree.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMPThree.length;
                        }

                        if (activosMPThree.length > 0) {
                            activosMPThreeP = assignmentsMPThree.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMPThree.length;
                        }

                        if (portadosMPThree.length > 0) {
                            portadosMPThreeP = assignmentsMPThree.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMPThree.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        all: [inactivosMPAllP, activosMPAllP, portadosMPAllP],
                        one: [inactivosMPOneP, activosMPOneP, portadosMPOneP],
                        three: [inactivosMPThreeP, activosMPThreeP, portadosMPThreeP],

                    });
                    break;

                case 'Mayorista secundario (sucursal)':
                    // Todo
                    const assignmentsMSAll = await ICCAssignment.find({ secondaryWholesaler: parameters.id });
                    let inactivosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMSAllP = 0;
                    let activosMSAllP = 0;
                    let portadosMSAllP = 0;

                    if (assignmentsMSAll.length > 0) {
                        if (inactivosMSAll.length > 0) {
                            inactivosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMSAll.length;
                        }

                        if (activosMSAll.length > 0) {
                            activosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMSAll.length;
                        }

                        if (portadosMSAll.length > 0) {
                            portadosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMSAll.length;
                        }
                    }

                    // Ultimo mes
                    const assignmentsMSOne = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: new Date(Date.now() - 2592000000), $lte: new Date() } });
                    let inactivosMSOne = assignmentsMSOne.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMSOne = assignmentsMSOne.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMSOne = assignmentsMSOne.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMSOneP = 0;
                    let activosMSOneP = 0;
                    let portadosMSOneP = 0;

                    console.log('Ultimo mes');
                    console.log(assignmentsMSOne);

                    if (assignmentsMSOne.length > 0) {
                        if (inactivosMSOne.length > 0) {
                            inactivosMSOneP = assignmentsMSOne.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMSOne.length;
                        }

                        if (activosMSOne.length > 0) {
                            activosMSOneP = assignmentsMSOne.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMSOne.length;
                        }

                        if (portadosMSOne.length > 0) {
                            portadosMSOneP = assignmentsMSOne.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMSOne.length;
                        }
                    }

                    // Ultimos tres meses
                    const assignmentsMSThree = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: new Date(Date.now() - 7776000000), $lte: new Date() } });
                    let inactivosMSThree = assignmentsMSThree.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMSThree = assignmentsMSThree.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMSThree = assignmentsMSThree.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMSThreeP = 0;
                    let activosMSThreeP = 0;
                    let portadosMSThreeP = 0;

                    console.log('Ultimos tres meses');
                    console.log(assignmentsMSThree);

                    if (assignmentsMSThree.length > 0) {
                        if (inactivosMSThree.length > 0) {
                            inactivosMSThreeP = assignmentsMSThree.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMSThree.length;
                        }

                        if (activosMSThree.length > 0) {
                            activosMSThreeP = assignmentsMSThree.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMSThree.length;
                        }

                        if (portadosMSThree.length > 0) {
                            portadosMSThreeP = assignmentsMSThree.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMSThree.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        all: [inactivosMSAllP, activosMSAllP, portadosMSAllP],
                        one: [inactivosMSOneP, activosMSOneP, portadosMSOneP],
                        three: [inactivosMSThreeP, activosMSThreeP, portadosMSThreeP],

                    });
                    break;
                case 'Distribuidor':
                    // Todo
                    const assignmentsDAll = await ICCAssignment.find({ distributor: parameters.id });
                    let inactivosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosDAllP = 0;
                    let activosDAllP = 0;
                    let portadosDAllP = 0;
                    console.log('Todo');
                    console.log(assignmentsDAll);

                    if (assignmentsDAll.length > 0) {
                        if (inactivosDAll.length > 0) {
                            inactivosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsDAll.length;
                        }

                        if (activosDAll.length > 0) {
                            activosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsDAll.length;
                        }

                        if (portadosDAll.length > 0) {
                            portadosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsDAll.length;
                        }
                    }

                    // Ultimo mes
                    const assignmentsDOne = await ICCAssignment.find({ distributor: parameters.id, $or: [{ sellerDate: { $gte: new Date(Date.now() - 2592000000), $lte: new Date() } }, { finalClientDate: { $gte: new Date(Date.now() - 2592000000), $lte: new Date() } }] });
                    let inactivosDOne = assignmentsDOne.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosDOne = assignmentsDOne.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosDOne = assignmentsDOne.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosDOneP = 0;
                    let activosDOneP = 0;
                    let portadosDOneP = 0;

                    console.log('Ultimo mes');
                    console.log(assignmentsDOne);

                    if (assignmentsDOne.length > 0) {
                        if (inactivosDOne.length > 0) {
                            inactivosDOneP = assignmentsDOne.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsDOne.length;
                        }

                        if (activosDOne.length > 0) {
                            activosDOneP = assignmentsDOne.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsDOne.length;
                        }

                        if (portadosDOne.length > 0) {
                            portadosDOneP = assignmentsDOne.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsDOne.length;
                        }
                    }

                    // Ultimos tres meses
                    const assignmentsDThree = await ICCAssignment.find({ distributor: parameters.id, $or: [{ sellerDate: { $gte: new Date(Date.now() - 7776000000), $lte: new Date() } }, { finalClientDate: { $gte: new Date(Date.now() - 7776000000), $lte: new Date() } }] });
                    let inactivosDThree = assignmentsDThree.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosDThree = assignmentsDThree.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosDThree = assignmentsDThree.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosDThreeP = 0;
                    let activosDThreeP = 0;
                    let portadosDThreeP = 0;

                    console.log('Ultimos tres meses');
                    console.log(assignmentsDThree);

                    if (assignmentsDThree.length > 0) {
                        if (inactivosDThree.length > 0) {
                            inactivosDThreeP = assignmentsDThree.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsDThree.length;
                        }

                        if (activosDThree.length > 0) {
                            activosDThreeP = assignmentsDThree.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsDThree.length;
                        }

                        if (portadosDThree.length > 0) {
                            portadosDThreeP = assignmentsDThree.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsDThree.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        all: [inactivosDAllP, activosDAllP, portadosDAllP],
                        one: [inactivosDOneP, activosDOneP, portadosDOneP],
                        three: [inactivosDThreeP, activosDThreeP, portadosDThreeP],

                    });
                    break;

                case 'Vendedor':
                    // Todo
                    const assignmentsVAll = await ICCAssignment.find({ seller: parameters.id });
                    let inactivosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosVAllP = 0;
                    let activosVAllP = 0;
                    let portadosVAllP = 0;
                    console.log('Todo');
                    console.log(assignmentsVAll);

                    if (assignmentsVAll.length > 0) {
                        if (inactivosVAll.length > 0) {
                            inactivosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsVAll.length;
                        }

                        if (activosVAll.length > 0) {
                            activosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsVAll.length;
                        }

                        if (portadosVAll.length > 0) {
                            portadosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsVAll.length;
                        }
                    }

                    // Ultimo mes
                    const assignmentsVOne = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: new Date(Date.now() - 2592000000), $lte: new Date() } });
                    let inactivosVOne = assignmentsVOne.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosVOne = assignmentsVOne.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosVOne = assignmentsVOne.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosVOneP = 0;
                    let activosVOneP = 0;
                    let portadosVOneP = 0;

                    console.log('Ultimo mes');
                    console.log(assignmentsVOne);

                    if (assignmentsVOne.length > 0) {
                        if (inactivosVOne.length > 0) {
                            inactivosVOneP = assignmentsVOne.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsVOne.length;
                        }

                        if (activosVOne.length > 0) {
                            activosVOneP = assignmentsVOne.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsVOne.length;
                        }

                        if (portadosVOne.length > 0) {
                            portadosVOneP = assignmentsVOne.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsVOne.length;
                        }
                    }

                    // Ultimos tres meses
                    const assignmentsVThree = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: new Date(Date.now() - 7776000000), $lte: new Date() } });
                    let inactivosVThree = assignmentsVThree.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosVThree = assignmentsVThree.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosVThree = assignmentsVThree.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosVThreeP = 0;
                    let activosVThreeP = 0;
                    let portadosVThreeP = 0;

                    console.log('Ultimos tres meses');
                    console.log(assignmentsVThree);

                    if (assignmentsVThree.length > 0) {
                        if (inactivosVThree.length > 0) {
                            inactivosVThreeP = assignmentsVThree.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsVThree.length;
                        }

                        if (activosVThree.length > 0) {
                            activosVThreeP = assignmentsVThree.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsVThree.length;
                        }

                        if (portadosVThree.length > 0) {
                            portadosVThreeP = assignmentsVThree.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsVThree.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        all: [inactivosVAllP, activosVAllP, portadosVAllP],
                        one: [inactivosVOneP, activosVOneP, portadosVOneP],
                        three: [inactivosVThreeP, activosVThreeP, portadosVThreeP],

                    });
                    break;
            }
        } else {
            return response.status(200).send({
                status: false,
                message: 'userType cannot be empty',
            });
        }
    },

    getLasts: async (request, response) => {
        const parameters = request.query;
        const meses = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"];
        let month = new Date().getUTCMonth();
        let year = new Date().getFullYear();
        let one = 0;
        let two = 0;
        let three = 0;
        let four = 0;
        let five = 0;
        let six = 0;
        let labelOne = '';
        let labelTwo = '';
        let labelThree = '';
        let labelFour = '';
        let labelFive = '';
        let labelSix = '';
        let consulta1 = null;
        let consulta2 = null;
        let start = `${year}-${month + 1}-01`;
        let end = `${year}-${month + 1}-31`;
        console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);


        switch (parameters.userType) {
            case 'Mayorista principal':
                consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                one = consulta1.length + consulta2.length;
                labelOne = meses[month];
                console.log(one);

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    two = consulta1.length + consulta2.length;
                    labelTwo = meses[12 + month];
                    console.log(two);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    two = consulta1.length + consulta2.length;
                    labelTwo = meses[month];
                    console.log(two);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    three = consulta1.length + consulta2.length;
                    labelThree = meses[12 + month];
                    console.log(three);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    three = consulta1.length + consulta2.length;
                    labelThree = meses[month];
                    console.log(three);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    four = consulta1.length + consulta2.length;
                    labelFour = meses[12 + month];
                    console.log(four);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    four = consulta1.length + consulta2.length;
                    labelFour = meses[month];
                    console.log(four);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    five = consulta1.length + consulta2.length;
                    labelFive = meses[12 + month];
                    console.log(five);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    five = consulta1.length + consulta2.length;
                    labelFive = meses[month];
                    console.log(five);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    six = consulta1.length + consulta2.length;
                    labelSix = meses[12 + month];
                    console.log(six);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesalerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ secondaryWholesaler: null, distributorDate: { $gte: start, $lte: end } });
                    six = consulta1.length + consulta2.length;
                    labelSix = meses[month];
                    console.log(six);
                }

                return response.status(200).send({
                    status: true,
                    message: 'lasts',
                    labels: [labelSix, labelFive, labelFour, labelThree, labelTwo, labelOne],
                    data: [six, five, four, three, two, one],
                });
                break;

            case 'Mayorista secundario (sucursal)':
                consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                one = consulta1.length;
                labelOne = meses[month];
                console.log(one);

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    two = consulta1.length;
                    labelTwo = meses[12 + month];
                    console.log(two);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    two = consulta1.length;
                    labelTwo = meses[month];
                    console.log(two);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    three = consulta1.length;
                    labelThree = meses[12 + month];
                    console.log(three);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    three = consulta1.length;
                    labelThree = meses[month];
                    console.log(three);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    four = consulta1.length;
                    labelFour = meses[12 + month];
                    console.log(four);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    four = consulta1.length;
                    labelFour = meses[month];
                    console.log(four);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    five = consulta1.length;
                    labelFive = meses[12 + month];
                    console.log(five);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    five = consulta1.length;
                    labelFive = meses[month];
                    console.log(five);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    six = consulta1.length;
                    labelSix = meses[12 + month];
                    console.log(six);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ secondaryWholesaler: parameters.id, distributorDate: { $gte: start, $lte: end } });
                    six = consulta1.length;
                    labelSix = meses[month];
                    console.log(six);
                }

                return response.status(200).send({
                    status: true,
                    message: 'lasts',
                    labels: [labelSix, labelFive, labelFour, labelThree, labelTwo, labelOne],
                    data: [six, five, four, three, two, one],
                });

                break;
            case 'Distribuidor':
                consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                console.log(consulta1);
                console.log(consulta2);
                one = consulta1.length + consulta2.length;
                labelOne = meses[month];
                console.log(one);

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    two = consulta1.length + consulta2.length;
                    labelTwo = meses[12 + month];
                    console.log(two);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    two = consulta1.length + consulta2.length;
                    labelTwo = meses[month];
                    console.log(two);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    three = consulta1.length + consulta2.length;
                    labelThree = meses[12 + month];
                    console.log(three);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    three = consulta1.length + consulta2.length;
                    labelThree = meses[month];
                    console.log(three);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    four = consulta1.length + consulta2.length;
                    labelFour = meses[12 + month];
                    console.log(four);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    four = consulta1.length + consulta2.length;
                    labelFour = meses[month];
                    console.log(four);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    five = consulta1.length + consulta2.length;
                    labelFive = meses[12 + month];
                    console.log(five);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    five = consulta1.length + consulta2.length;
                    labelFive = meses[month];
                    console.log(five);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    six = consulta1.length + consulta2.length;
                    labelSix = meses[12 + month];
                    console.log(six);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ distributor: parameters.id, sellerDate: { $gte: start, $lte: end } });
                    consulta2 = await ICCAssignment.find({ distributor: parameters.id, seller: null, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    console.log(consulta2);
                    six = consulta1.length + consulta2.length;
                    labelSix = meses[month];
                    console.log(six);
                }

                return response.status(200).send({
                    status: true,
                    message: 'lasts',
                    labels: [labelSix, labelFive, labelFour, labelThree, labelTwo, labelOne],
                    data: [six, five, four, three, two, one],
                });

                break;

            case 'Vendedor':
                consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                console.log(consulta1);
                one = consulta1.length;
                labelOne = meses[month];
                console.log(one);

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    two = consulta1.length;
                    labelTwo = meses[12 + month];
                    console.log(two);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    two = consulta1.length;
                    labelTwo = meses[month];
                    console.log(two);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    three = consulta1.length;
                    labelThree = meses[12 + month];
                    console.log(three);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    three = consulta1.length;
                    labelThree = meses[month];
                    console.log(three);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    four = consulta1.length;
                    labelFour = meses[12 + month];
                    console.log(four);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    four = consulta1.length;
                    labelFour = meses[month];
                    console.log(four);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    five = consulta1.length;
                    labelFive = meses[12 + month];
                    console.log(five);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    five = consulta1.length;
                    labelFive = meses[month];
                    console.log(five);
                }

                month--;

                if (month < 0) {
                    start = `${year - 1}-${12 + month + 1}-01`;
                    end = `${year - 1}-${12 + month + 1}-31`;
                    console.log(`${year - 1}-${12 + month + 1}-01 a ${year - 1}-${12 + month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    six = consulta1.length;
                    labelSix = meses[12 + month];
                    console.log(six);
                } else {
                    start = `${year}-${month + 1}-01`;
                    end = `${year}-${month + 1}-31`;
                    console.log(`${year}-${month + 1}-01 a ${year}-${month + 1}-31`);
                    consulta1 = await ICCAssignment.find({ seller: parameters.id, finalClientDate: { $gte: start, $lte: end } });
                    console.log(consulta1);
                    six = consulta1.length;
                    labelSix = meses[month];
                    console.log(six);
                }

                return response.status(200).send({
                    status: true,
                    message: 'lasts',
                    labels: [labelSix, labelFive, labelFour, labelThree, labelTwo, labelOne],
                    data: [six, five, four, three, two, one],
                });

                break;
        }

        return response.status(200).send({
            message: 'statistics controller test'
        });
    },

    getStatisticsDashboard: async (request, response) => {
        const parameters = request.body;

        if (parameters.id) {

        } else {
            return response.status(200).send({
                status: false,
                message: 'id cannot be empty',
            });
        }


        if (parameters.userType) {
            switch (parameters.userType) {
                case 'Mayorista principal':
                    // Todo
                    const assignmentsMPAll = await ICCAssignment.find();
                    let inactivosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMPAllP = 0;
                    let activosMPAllP = 0;
                    let portadosMPAllP = 0;

                    if (assignmentsMPAll.length > 0) {
                        if (inactivosMPAll.length > 0) {
                            inactivosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMPAll.length;
                        }

                        if (activosMPAll.length > 0) {
                            activosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMPAll.length;
                        }

                        if (portadosMPAll.length > 0) {
                            portadosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMPAll.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        inactivosMPAllP: inactivosMPAllP * 100,
                        activosMPAllP: activosMPAllP * 100,
                        portadosMPAllP: portadosMPAllP * 100,
                        inactivosMPAll: inactivosMPAll.length,
                        activosMPAll: activosMPAll.length,
                        portadosMPAll: portadosMPAll.length,
                        total: inactivosMPAll.length + activosMPAll.length + portadosMPAll.length

                    });
                    break;

                case 'Mayorista secundario (sucursal)':
                    // Todo
                    const assignmentsMSAll = await ICCAssignment.find({ secondaryWholesaler: parameters.id });
                    let inactivosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosMSAllP = 0;
                    let activosMSAllP = 0;
                    let portadosMSAllP = 0;

                    if (assignmentsMSAll.length > 0) {
                        if (inactivosMSAll.length > 0) {
                            inactivosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMSAll.length;
                        }

                        if (activosMSAll.length > 0) {
                            activosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMSAll.length;
                        }

                        if (portadosMSAll.length > 0) {
                            portadosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMSAll.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        inactivosMSAllP: inactivosMSAllP * 100,
                        activosMSAllP: activosMSAllP * 100,
                        portadosMSAllP: portadosMSAllP * 100,
                        inactivosMSAll: inactivosMSAll.length,
                        activosMSAll: activosMSAll.length,
                        portadosMSAll: portadosMSAll.length,
                        total: inactivosMSAll.length + activosMSAll.length + portadosMSAll.length

                    });
                    break;
                case 'Distribuidor':
                    // Todo
                    const assignmentsDAll = await ICCAssignment.find({ distributor: parameters.id });
                    let inactivosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosDAllP = 0;
                    let activosDAllP = 0;
                    let portadosDAllP = 0;
                    console.log('Todo');
                    console.log(assignmentsDAll);

                    if (assignmentsDAll.length > 0) {
                        if (inactivosDAll.length > 0) {
                            inactivosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsDAll.length;
                        }

                        if (assignmentsDAll.length > 0) {
                            activosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsDAll.length;
                        }

                        if (assignmentsDAll.length > 0) {
                            portadosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsDAll.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        inactivosDAllP: inactivosDAllP * 100,
                        activosDAllP: activosDAllP * 100,
                        portadosDAllP: portadosDAllP * 100,
                        inactivosDAll: inactivosDAll.length,
                        activosDAll: activosDAll.length,
                        portadosDAll: portadosDAll.length,
                        total: inactivosDAll.length + activosDAll.length + portadosDAll.length

                    });
                    break;

                case 'Vendedor':
                    // Todo
                    const assignmentsVAll = await ICCAssignment.find({ seller: parameters.id });
                    let inactivosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosVAllP = 0;
                    let activosVAllP = 0;
                    let portadosVAllP = 0;
                    console.log('Todo');
                    console.log(assignmentsVAll);

                    if (assignmentsVAll.length > 0) {
                        if (inactivosVAll.length > 0) {
                            inactivosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsVAll.length;
                        }

                        if (assignmentsVAll.length > 0) {
                            activosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsVAll.length;
                        }

                        if (assignmentsVAll.length > 0) {
                            portadosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsVAll.length;
                        }
                    }

                    return response.status(200).send({
                        status: true,
                        message: 'statistics',
                        inactivosVAllP: inactivosVAllP * 100,
                        activosVAllP: activosVAllP * 100,
                        portadosVAllP: portadosVAllP * 100,
                        inactivosVAll: inactivosVAll.length,
                        activosVAll: activosVAll.length,
                        portadosVAll: portadosVAll.length,
                        total: inactivosVAll.length + activosVAll.length + portadosVAll.length

                    });
                    break;
            }
        } else {
            return response.status(200).send({
                status: false,
                message: 'userType cannot be empty',
            });
        }
    },
};

module.exports = controller;