var nodemailer = require("nodemailer");

// email sender function
exports.sendEmail = function(req, res) {
  // Definimos el transporter
  var transporter = nodemailer.createTransport({
    service: "Gmail",
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "saradevs25@gmail.com",
      pass: "s4r41251189"
    }
  });

  console.log("SMTP Configured");

  // Definimos el email
  var mailOptions = {
    from: "Remitente",
    to: "saradevs25@gmail.com",
    subject: "Asunto",
    text: "Contenido del email"
  };
  // Enviamos el email
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
      res.send(500, error.message);
    } else {
      console.log("Email sent");
      res.status(200).jsonp(req.body);
    }
  });
};

//  // create reusable transporter object using the default SMTP transport
// let mailSender = {
//     sendMail: async(recivers, body) => {

//         // create reusable transporter object using the default SMTP transport
//         let transporter = nodemailer.createTransport({
//             host: "smtp.1und1.de",
//             port: 25,
//             secure: false, // true for 465, false for other ports
//             auth: {
//                 user: 'mailtest@hersymac.com', // generated ethereal user
//                 pass: 'Megustanlosgatos123!' // generated ethereal password
//             }
//         });

//         var mailOptions = {
//             from: '"Correo de prueba Hersymac" <mailtest@hersymac.com>', // sender address
//             to: recivers, // list of receivers
//             subject: "Prueba de envio de correos", // Subject line
//             text: "", // plain text body (optional)
//             // attachments: attach
//         }

//         // send mail with defined transport object
//         let info = await transporter.sendMail(mailOptions);

//         console.log('accepted', info.accepted);
//         console.log('rejected', info.rejected);
//         console.log('envelopeTime', info.envelopeTime);
//         console.log('messageTime', info.messageTime);
//         console.log('messageSize', info.messageSize);
//         console.log('response', info.response);
//         console.log('envelope', info.envelope);
//         console.log('messageId', info.messageId);
//     }
// }

// module.exports = mailSender;

// exports.sendEmail = function(req, res){
// // Definimos el transporter
//     var transporter = nodemailer.createTransport({
//         host: "smtp.1und1.de",
//         port: 25,
//         secure: false, // true for 465, false for other ports
//         auth: {
//             user: 'mailtest@hersymac.com', // generated ethereal user
//             pass: 'Megustanlosgatos123!' // generated ethereal password
//         }
//     });
// // Definimos el email
// var mailOptions = {
//     from: '"Correo de prueba Hersymac" <mailtest@hersymac.com>', // sender address
//     to: 'sfalconp43@gmail.com', // list of receivers
//     subject: "Prueba de envio de correos", // Subject line
//     text: "Hola que hace", // plain text body (optional)
//     // attachments: attach
// };
// // Enviamos el email
// transporter.sendMail(mailOptions, function(error, info){
//     if (error){
//         console.log(error);
//         res.send(500, error.message);
//     } else {
//         console.log("Email sent");
//         res.status(200).jsonp(req.body);
//     }
// });
// };
