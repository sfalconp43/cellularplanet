'use strict'

const Redemption = require('../models/redemption');
const Penalty = require('../models/penalty');
const Reward = require('../models/reward');
const Distributor = require('../models/distributor');

const ICCAssignment = require('../models/ICCAssignment');
// const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        return response.status(200).send({
            message: 'redemption controller test'
        });
    },

    createRedemption: async (request, response) => {
        const parameters = request.body
        var redemption = new Redemption();


        if (parameters.distributor) {
            redemption.distributor = parameters.distributor;
        } else {

            request.flash('error-message', 'distributor no puede estar vacío');
            return response.status(200).redirect('redencion-de-puntos');
            // return response.status(200).send({
            //     status: false,
            //     message: 'distributor cannot be empty',
            // });
        }

        if (parameters.redemptionPoints) {
            redemption.redemptionPoints = parameters.redemptionPoints;
        } else {
            request.flash('error-message', 'Puntos de redención no puede estar vacío');
            return response.status(200).redirect('redencion-de-puntos');
            // return response.status(200).send({
            //     status: false,
            //     message: 'redemptionPoints cannot be empty',
            // });
        }

        if (parameters.description) {
            redemption.description = parameters.description;
        }

        const redemptionStored = await redemption.save();

        request.flash('success-message', 'Redención creada de forma exitosa');
            return response.status(200).redirect('redencion-de-puntos');

        // return response.status(200).send({
        //     status: true,
        //     message: 'redemption created',
        //     redemptionStored: redemptionStored
        // });
    },

    acceptRedemption: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.redemption) {
            update.status = 'Aceptada';
        } else {
            request.flash('error-message', 'redencion no puede estar vacía');
            return response.status(200).redirect('redencion-de-puntos');

            // return response.status(200).send({
            //     status: false,
            //     message: 'redemption cannot be empty',
            // });
        }

        const redemption = await Redemption.findOne({ _id: parameters.redemption });

        if (!redemption) {

            request.flash('error-message', 'redencion no encontrada');
            return response.status(200).redirect('redencion-de-puntos');
            // return response.status(200).send({
            //     status: false,
            //     message: 'redemption not found',
            // });
        } else {
            const redemptionAccepted = await Redemption.findOneAndUpdate({ _id: parameters.redemption }, update, { new: true });

            request.flash('success-message', 'Redención aceptada de forma exitosa');
            return response.status(200).redirect('redencion-de-puntos-distribuidor');

            // return response.status(200).send({
            //     status: true,
            //     message: 'redemption accepted',
            //     redemptionAccepted: redemptionAccepted
            // });
        }
    },

    cancelRedemption: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.redemption) {
            update.status = 'Cancelada';
        } else {
            request.flash('error-message', 'redencion no puede estar vacía');
            return response.status(200).redirect('redencion-de-puntos');
            // return response.status(200).send({
            //     status: false,
            //     message: 'redemption cannot be empty',
            // });
        }

        const redemption = await Redemption.findOne({ _id: parameters.redemption });

        if (!redemption) {
            request.flash('error-message', 'redencion no encontrada');
            return response.status(200).redirect('redencion-de-puntos');
            
            // return response.status(200).send({
            //     status: false,
            //     message: 'redemption not found',
            // });
        } else {
            const redemptionAccepted = await Redemption.findOneAndUpdate({ _id: parameters.redemption }, update, { new: true });

            request.flash('success-message', 'Redención aceptada');
            return response.status(200).redirect('redencion-de-puntos-distribuidor');
            // return response.status(200).send({
            //     status: true,
            //     message: 'redemption canceled',
            //     redemptionAccepted: redemptionAccepted
            // });
        }
    },

    getRedemptions: async (request, response) => {
        const parameters = request.body

        if (parameters.distributor) {
            if (parameters.status) {
                switch (parameters.status) {
                    case 'En espera':
                        const waitingRedemptions = await Redemption.find({ distributor: parameters.distributor, status: parameters.status });
                        return response.status(200).send({
                            status: true,
                            message: 'list of redemptions',
                            waitingRedemptions: waitingRedemptions
                        });
                        break;
                    case 'Aceptada':
                        const redemptionsAccepted = await Redemption.find({ distributor: parameters.distributor, status: parameters.status });
                        return response.status(200).send({
                            status: true,
                            message: 'list of redemptions',
                            redemptionsAccepted: redemptionsAccepted
                        });
                        break;
                    case 'Cancelada':
                        const redemptionsCanceled = await Redemption.find({ distributor: parameters.distributor, status: parameters.status });
                        return response.status(200).send({
                            status: true,
                            message: 'list of redemptions',
                            redemptionsCanceled: redemptionsCanceled
                        });
                        break;

                    default:
                        const redemptions = await Redemption.find({ distributor: parameters.distributor });
                        return response.status(200).send({
                            status: true,
                            message: 'list of redemptions',
                            redemptions: redemptions
                        });
                        break;
                }
            } else {
                const redemptions = await Redemption.find({ distributor: parameters.distributor });
                return response.status(200).send({
                    status: true,
                    message: 'list of redemptions',
                    redemptions: redemptions
                });
            }
        } else {
            const redemptions = await Redemption.find();
            return response.status(200).send({
                status: true,
                message: 'list of redemptions',
                redemptions: redemptions
            });
        }
    },

    getPoints: async (request, response) => {
        const parameters = request.body;
        let waitingPoints = 0;
        let pointsAvailable = 0;
        let redeemedPoints = 0;
        const mes = 2592000000;
        let rewardsCount = 0;
        let penaltiesCount = 0;
        let redemptionCount = 0;

        if (parameters.distributor) {
            // console.log('ingreso aqui con id', parameters._id);
            const distributor = await Distributor.findOne({_id: parameters.distributor});
            console.log('AQUI distributor', parameters.distributor);
            let distributorDB = distributor.toObject();
            delete distributorDB.password;
            const rewards = await Reward.find({distributor: parameters.distributor});
            console.log('rewarrs', rewards);
            const penalties = await Penalty.find({distributor: parameters.distributor});
            console.log('penalty', penalties);
            const redemptions = await Redemption.find({distributor: parameters.distributor, status: 'Aceptada'});
            console.log('redemptions', redemptions);

            rewards.forEach(element => {
                if (new Date().getTime() - element.dateCreated.getTime() >= mes) {
                    pointsAvailable = pointsAvailable + element.rewardPoints;
                } else {
                    waitingPoints = waitingPoints + element.rewardPoints;
                }

                rewardsCount++;

                if (rewardsCount === rewards.length && penaltiesCount === penalties.length && redemptionCount === redemptions.length) {


                    return response.status(200).render('distribuidor-detalles',
                    {
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    }
                    
                    );

                    

                    // return response.status(200).send({
                    //     status: true,
                    //     message: 'distributor points and info',
                    //     distributor: distributorDB,
                    //     waitingPoints: waitingPoints,
                    //     pointsAvailable: pointsAvailable,
                    //     redeemedPoints: redeemedPoints
                    // });
                }
            });

            penalties.forEach(element => {
                pointsAvailable = pointsAvailable - element.penaltyPoints;
                penaltiesCount++;

                if (rewardsCount === rewards.length && penaltiesCount === penalties.length && redemptionCount === redemptions.length) {

                    return response.status(200).render('distribuidor-detalles',
                    {
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    }
                    
                    );
                    
                    // return response.status(200).send({
                    //     status: true,
                    //     message: 'distributor points and info',
                    //     distributor: distributorDB,
                    //     waitingPoints: waitingPoints,
                    //     pointsAvailable: pointsAvailable,
                    //     redeemedPoints: redeemedPoints
                    // });
                }
            });

            redemptions.forEach(element => {
                redeemedPoints = redeemedPoints + element.redemptionPoints;
                redemptionCount++;

                if (rewardsCount === rewards.length && penaltiesCount === penalties.length && redemptionCount === redemptions.length) {

                    // request.flash('success-message', 'distributor points and info');
                    return response.status(200).render('distribuidor-detalles',
                    {
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    }
                    
                    );
                }
            });

            if (rewards.length === 0 && penalties.length === 0 && redemptions.length === 0) {
                return response.status(200).render('distribuidor-detalles',
                    {
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    }
                    
                    );
                
            }

        } else {
            return response.status(200).send({
                status: false,
                message: 'distributor cannot be empty',
            });
        }
    },
};

module.exports = controller;