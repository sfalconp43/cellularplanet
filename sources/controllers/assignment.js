"use strict";

const Assignment = require("../models/assignment");
const bcrypt = require("bcrypt");
const config = require("../config");

var controller = {
  test: (request, response) => {
    console.log(request.body);
    return response.status(200).send({
      message: "Whole Saler controller test"
    });
  },

  saveAssignment: async (request, response) => {
    console.log(request);
    const parameters = request.body;
    var assigmentTo = new Assignment();

    assigmentTo.iccsim = parameters.iccsim;
    assigmentTo.nameAssigning = parameters.name1;
    assigmentTo.lastnameAssigning = parameters.lastname1;
    assigmentTo.nameAssigned = parameters.name2;
    assigmentTo.lastnameAssigned = parameters.lastname2;
    assigmentTo.buyerPhone = parameters.phone;
    assigmentTo.idUserAssign = parameters.idUserAssign;
    assigmentTo.idUserAssigned = parameters.idUserAssigned;
    assigmentTo.ifAssigingWholeSaler = parameters.ifAssigingWholeSaler;
    assigmentTo.ifAssigningDistributor = parameters.ifAssigningDistributor;
    assigmentTo.ifAssigningSeller = parameters.ifAssigningSeller;
    assigmentTo.ifAssignedWholeSaler = parameters.ifAssignedWholeSaler;
    assigmentTo.ifAssignedFinalUser = parameters.ifAssignedFinalUser;
    assigmentTo.ifAssignedDistributor = parameters.ifAssignedMassively;
    assigmentTo.ifAssignedSeller = parameters.ifAssignedSeller;
    assigmentTo.ifAssignedMassively = parameters.ifAssignedMassively;
    assigmentTo.status = parameters.status;
    assigmentTo.userType = parameters.userType;

    let user = parameters.userType;

    switch (user) {
      case "Mayorista":
        const assigmentToAux = await Assignment.findOne({
          iccsim: assigmentTo.iccsim
        });
        //assigmentTo.status = 'inactivo';
        let massive = parameters.ifAssignedMassively;
        console.log(massive);

        if (massive === "true") {
          assigmentTo.status = "pre - activo";
        } else {
          assigmentTo.status = "inactivo";
        }

        if (assigmentToAux) {
          const assigmentToStored = await assigmentTo.save();

          return response.status(200).send({
            status: true,
            message: "Registered user successfully",
            assigmentToStored: assigmentToStored
          });
        } else {
          const assigmentToStored = await assigmentTo.save();
          return response.status(200).send({
            status: false,
            message: "Registered new user successfully",
            assigmentToStored: assigmentToStored
          });
        }

        break;

        case "Mayorista Sucursal":
          const assigmentToAux4 = await Assignment.findOne({
            iccsim: assigmentTo.iccsim
          });
          //assigmentTo.status = 'inactivo';
          let massive4 = parameters.ifAssignedMassively;
          console.log(massive4);
  
          if (massive4 === "true") {
            assigmentTo.status = "pre - activo";
          } else {
            assigmentTo.status = "inactivo";
          }
  
          if (assigmentToAux4) {
            const assigmentToStored = await assigmentTo.save();
  
            return response.status(200).send({
              status: true,
              message: "Registered user successfully",
              assigmentToStored: assigmentToStored
            });
          } else {
            const assigmentToStored = await assigmentTo.save();
            return response.status(200).send({
              status: false,
              message: "Registered new user successfully",
              assigmentToStored: assigmentToStored
            });
          }
  
          break;    

      case "Distribuidor":
        const assigmentToAux2 = await Assignment.findOne({
          iccsim: assigmentTo.iccsim
        });
        //assigmentTo.status = 'inactivo';
        let massive2 = parameters.ifAssignedMassively;

        if (massive2 === "true") {
          assigmentTo.status = "pre activo";
        } else {
          assigmentTo.status = "activo";
        }

        if (assigmentToAux2) {
          const assigmentToStored = await assigmentTo.save();

          return response.status(200).send({
            status: true,
            message: "Registered user successfully",
            assigmentToStored: assigmentToStored
          });
        } else {
          const assigmentToStored = await assigmentTo.save();
          return response.status(200).send({
            status: false,
            message: "Registered new user successfully",
            assigmentToStored: assigmentToStored
          });
        }

        break;

      case "Vendedor":
        const assigmentToAux3 = await Assignment.findOne({
          iccsim: assigmentTo.iccsim
        });
        //assigmentTo.status = 'inactivo';
        let massive3 = parameters.ifAssignedMassively;

        if (massive3 === "false") {
          assigmentTo.status = "activo";
        } else {
          assigmentTo.status = "activo";
        }

        if (assigmentToAux3) {
          const assigmentToStored = await assigmentTo.save();
          return response.status(200).send({
            status: true,
            message: "Registered user successfully",
            assigmentToStored: assigmentToStored
          });
        } else {
          const assigmentToStored = await assigmentTo.save();
          return response.status(200).send({
            status: false,
            message: "Registered user successfully",
            assigmentToStored: assigmentToStored
          });
        }

        break;

      default:
        console.log("No existe ese tipo de usuario");
        break;
    }
  },

  saveAssignmentWeb: async (request, response) => {
    console.log(request);
    const parameters = request.body;
    var assigmentTo = new Assignment();

    assigmentTo.iccsim = parameters.iccsim;
    assigmentTo.nameAssigning = parameters.name1;
    assigmentTo.lastnameAssigning = parameters.lastname1;
    assigmentTo.nameAssigned = parameters.name2;
    assigmentTo.lastnameAssigned = parameters.lastname2;
    assigmentTo.buyerPhone = parameters.phone;
    assigmentTo.idUserAssign = parameters.idUserAssign;
    assigmentTo.idUserAssigned = parameters.idUserAssigned;
    assigmentTo.ifAssigingWholeSaler = parameters.ifAssigingWholeSaler;
    assigmentTo.ifAssigningDistributor = parameters.ifAssigningDistributor;
    assigmentTo.ifAssigningSeller = parameters.ifAssigningSeller;
    assigmentTo.ifAssignedWholeSaler = parameters.ifAssignedWholeSaler;
    assigmentTo.ifAssignedFinalUser = parameters.ifAssignedFinalUser;
    assigmentTo.ifAssignedDistributor = parameters.ifAssignedMassively;
    assigmentTo.ifAssignedSeller = parameters.ifAssignedSeller;
    assigmentTo.ifAssignedMassively = parameters.ifAssignedMassively;
    assigmentTo.status = parameters.status;
    assigmentTo.userType = parameters.userType;

    let user = parameters.userType;

    switch (user) {
      case "Mayorista":
        const assigmentToAux = await Assignment.findOne({
          iccsim: assigmentTo.iccsim
        });
        //assigmentTo.status = 'inactivo';
        let massive = parameters.ifAssignedMassively;
        console.log(massive);

        if (massive === "true") {
          assigmentTo.status = "pre - activo";
        } else {
          assigmentTo.status = "inactivo";
        }

        if (assigmentToAux) {

          const assigmentToStored = await assigmentTo.save();
          request.flash('success-message', 'Asignacion guardado de forma exitosa');
          return response.status(200).redirect('asignacion-a-usuario-sucursal');

          // return response.status(200).send({
          //   status: true,
          //   message: "Registered user successfully",
          //   assigmentToStored: assigmentToStored
          // });
        } else {
          const assigmentToStored = await assigmentTo.save();
          request.flash('success-message', 'Asignacion guardado de forma exitosa');
          return response.status(200).redirect('asignacion-a-usuario-sucursal');
          // return response.status(200).send({
          //   status: true,
          //   message: "Registered new user successfully",
          //   assigmentToStored: assigmentToStored
          // });
        }

        break;

      case "Distribuidor":
        const assigmentToAux2 = await Assignment.findOne({
          iccsim: assigmentTo.iccsim
        });
        //assigmentTo.status = 'inactivo';
        let massive2 = parameters.ifAssignedMassively;

        if (massive2 === "true") {
          assigmentTo.status = "pre activo";
        } else {
          assigmentTo.status = "activo";
        }

        if (assigmentToAux2) {
          const assigmentToStored = await assigmentTo.save();
          request.flash('success-message', 'Asignacion guardada de forma exitosa');
          return response.status(200).redirect('asignacion-a-usuario-distribuidor-ms');

        } else {
          const assigmentToStored = await assigmentTo.save();
          request.flash('success-message', 'Asignacion guardada de forma exitosa');
          return response.status(200).redirect('asignacion-a-usuario-distribuidor-ms');
        }

        break;

      case "Vendedor":
        const assigmentToAux3 = await Assignment.findOne({
          iccsim: assigmentTo.iccsim
        });
        //assigmentTo.status = 'inactivo';
        let massive3 = parameters.ifAssignedMassively;

        if (massive3 === "false") {
          assigmentTo.status = "activo";
        } else {
          assigmentTo.status = "activo";
        }

        if (assigmentToAux3) {
          const assigmentToStored = await assigmentTo.save();
          request.flash('success-message', 'Asignacion guardado de forma exitosa');
          return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
          // return response.status(200).send({
          //   status: true,
          //   message: "Registered user successfully",
          //   assigmentToStored: assigmentToStored
          // });
        } else {
          const assigmentToStored = await assigmentTo.save();
          request.flash('success-message', 'Asignacion guardado de forma exitosa');
          return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
        }

        break;

      default:
        console.log("No existe ese tipo de usuario");
        break;
    }
  },


  updateAssignment: async (request, response) => {
    let update = {};
    const parameters = request.body;
    const AssignmentId = parameters.id;

    if (parameters.iccsim) {
      update.iccsim = parameters.iccsim;
    }

    if (parameters.name) {
      update.name = parameters.name;
    }

    if (parameters.lastname) {
      update.lastname = parameters.lastname;
    }

    if (parameters.idUserAssign) {
      update.idUserAssign = parameters.idUserAssign;
    }

    if (parameters.idUserAssigned) {
      update.idUserAssigned = parameters.idUserAssigned;
    }

    if (parameters.ifAssignedWholeSaler) {
      update.ifAssignedWholeSaler = parameters.ifAssignedWholeSaler;
    }

    if (parameters.ifAssignedFinalUser) {
      update.ifAssignedFinalUser = parameters.ifAssignedFinalUser;
    }

    if (parameters.ifAssignedDistributor) {
      update.ifAssignedDistributor = parameters.ifAssignedDistributor;
    }

    if (parameters.ifAssignedMassively) {
      update.ifAssignedMassively = parameters.ifAssignedMassively;
    }

    if (parameters.status) {
      update.status = parameters.status;
    }

    const assignmentUpdate = await Assignment.findByIdAndUpdate(
      AssignmentId,
      update,
      { new: true }
    );

    if (!assignmentUpdate) {
      return response.status(200).send({
        status: false,
        message: "User not found",
        assignmentUpdate: null
      });
    } else {
      return response.status(200).send({
        status: true,
        message: "Updated user",
        assignmentUpdate: assignmentUpdate
      });
    }
  },

  updateAssignmentWeb: async (request, response) => {
    let update = {};
    const parameters = request.body;
    const AssignmentId = parameters.id;

    if (parameters.iccsim) {
      update.iccsim = parameters.iccsim;
    }

    if (parameters.name) {
      update.name = parameters.name2;
    }

    if (parameters.lastname) {
      update.lastname = parameters.lastname;
    }

    if (parameters.idUserAssign) {
      update.idUserAssign = parameters.idUserAssign;
    }

    if (parameters.idUserAssigned) {
      update.idUserAssigned = parameters.idUserAssigned;
    }

    if (parameters.ifAssignedWholeSaler) {
      update.ifAssignedWholeSaler = parameters.ifAssignedWholeSaler;
    }

    if (parameters.ifAssignedFinalUser) {
      update.ifAssignedFinalUser = parameters.ifAssignedFinalUser;
    }

    if (parameters.ifAssignedDistributor) {
      update.ifAssignedDistributor = parameters.ifAssignedDistributor;
    }

    if (parameters.ifAssignedMassively) {
      update.ifAssignedMassively = parameters.ifAssignedMassively;
    }

    if (parameters.status) {
      update.status = parameters.status;
    }

    const assignmentUpdate = await Assignment.findByIdAndUpdate(
      AssignmentId,
      update,
      { new: true }
    );

    if (!assignmentUpdate) {
      return response.status(200).send({
        status: false,
        message: "User not found",
        assignmentUpdate: null
      });
    } else {
      return response.status(200).send({
        status: true,
        message: "Updated user",
        assignmentUpdate: assignmentUpdate
      });
    }
  },


  getAssignment: async (request, response) => {
    const parameters = request.body;
    var assignmentId = parameters.id;

    const assignmentUser = await Assignment.findById(assignmentId);

    if (!assignmentUser) {
      return response.status(200).send({
        status: false,
        message: "User not found"
      });
    } else {
      let assignmentDB = assignmentUser.toObject();
      //delete wholeSalerDB.password;

      return response.status(200).send({
        status: true,
        message: "User found",
        assignmentDB: assignmentDB
      });
    }
  },

  getUsersById2: async (request, response) => {
    const parameters = request.body;
    var idUserAssign = parameters.id;
    const assignmentUsers = await Assignment.find({
      idUserAssign: idUserAssign
    });
    //console.log(assignmentUsers);

    if (assignmentUsers.length == 0) {
      return response.status(200).send({
        status: false,
        message: "User not found",
        assignmentUserDelete: null
      });
    } else {
      let assignmentUsersDB = [];

      assignmentUsers.forEach(assignmentUser => {
        let assigmentUserDB = assignmentUser.toObject();
        assignmentUsersDB.push(assigmentUserDB);
      });

      return response.status(200).send({
        status: true,
        message: "list of whole salers",
        assignmentUsers: assignmentUsersDB
      });
    }
  },

  getAssignments: async (request, response) => {
    const assignmentUsers = await Assignment.find({});
    let assignmentUsersDB = [];

    assignmentUsers.forEach(assignmentUser => {
      let assigmentUserDB = assignmentUser.toObject();
      assignmentUsersDB.push(assigmentUserDB);
    });

    return response.status(200).send({
      status: true,
      message: "list of whole salers",
      assignmentUsers: assignmentUsersDB
    });
  },

  deleteAssignmentUser: async (request, response) => {
    const parameters = request.body;
    const assignmentUserId = parameters.id;

    const assignmentUserDelete = await Assignment.findByIdAndRemove(
      assignmentUserId
    );

    if (!assignmentUserDelete) {
      return response.status(200).send({
        status: false,
        message: "User not found",
        assignmentUserDelete: null
      });
    } else {
      return response.status(200).send({
        status: true,
        message: "deleted user",
        assignmentUserDelete: assignmentUserDelete
      });
    }
  },

  deleteAssignmentUserWeb: async (request, response) => {
    const parameters = request.body;
    const assignmentUserId = parameters.id;

    const assignmentUserDelete = await Assignment.findByIdAndRemove(
      assignmentUserId
    );



    if (!assignmentUserDelete) {
      request.flash('error-message', 'Error al borrar la asignacion seleccionada');
      return response.status(200).redirect('asignacion-a-usuario-sucursal');
  } else {
      request.flash('success-message', 'Asignacion eliminada de forma exitosa');
      return response.status(200).redirect('asignacion-a-usuario-sucursal');
  }

    // if (!assignmentUserDelete) {
    //   return response.status(200).send({
    //     status: false,
    //     message: "User not found",
    //     assignmentUserDelete: null
    //   });
    // } else {
    //   return response.status(200).send({
    //     status: true,
    //     message: "deleted user",
    //     assignmentUserDelete: assignmentUserDelete
    //   });
    // }
  },

  pointsDistributor: async (request, response) => {
    const parameters = request.body;

    var typeUser = parameters.type;

    var typeUser2 = parameters.type2;

    const assignmentUsers = await Assignment.find({ userType: typeUser });
    const assignmentSellers = await Assignment.find({ userType: typeUser2 });

    if (assignmentUsers) {
      // const pointToDist = await Assignment.find({iccsim: iccSeller});

      let assignmentUsersDB = [];

      let assignmentSellersDB = [];

      let total = [];

      let total2 = [];


      let dateToday = new Date();

      let date2 = dateToday.getTime(); // fecha en milisegundos

      console.log("fecha actual", date2);

      var milliseconds = 2592000000; // 30 días en milisegundos

      // let date3 = date2 - milliseconds;
      // // console.log("cantidad a evaluar", date3);

      assignmentUsers.forEach(assignmentUser => {
        let assigmentUserDB = assignmentUser.toObject(); // todos los distribuidores

        assignmentUsersDB.push(assigmentUserDB);


        // var t = assignmentUsersDB;
        // console.log(t.length);

        var seller = assigmentUserDB.ifAssigningSeller;

        var finalUser = assigmentUserDB.ifAssignedFinalUser; // ventas a usuario final de distribuidores

        var iccDist = assigmentUserDB.iccsim;

        var nameDist = assigmentUserDB.nameAssigning;

        var idDistDirect = assigmentUserDB.idUserAssign;

        let inicialDate = assigmentUserDB.dateCreated; // fecha de venta icc por parte de un distribuidor

        let dateToSeconds = inicialDate.getTime(); // conversion de fecha venta a milisegundos

        let date4 = date2 - dateToSeconds;
        // console.log('resultado de fecha a evaluar', date4);

        let totalPointsDist = 100;
        
        let pointsSwapDist = 50;

        let NewTotal = totalPointsDist - pointsSwapDist;


        if (seller == false) {
          if (date4 >= milliseconds) {
            // console.log('Usuario Distribuidor tienes disponibles', pointsDistAvailable, 'puntos tu user es', nameDist);
            console.log("tienes x puntos disponibles tu user es", nameDist);
            // total.push(nameDist);
            // console.log(total);
            // console.log(total.length); //total se puede multiplicar x total puntos estipulados, ejem total x 100pts

          } else if (date4 <= milliseconds) {
            console.log("tienes x puntos en espera tu user es", nameDist);
          } else {
            console.log("No puede acceder a asignacion de puntos");
          }
        } else if (seller == true) {
          if (assignmentSellers) {
            // de los vendedores asignados cuales han vendido a usuarios finales

            assignmentSellers.forEach(assignmentSeller => {

              let assigmentSellerDB = assignmentSeller.toObject(); // todos los vendedores

              assignmentSellersDB.push(assigmentSellerDB);

              var iccSeller = assigmentSellerDB.iccsim;

              var inicialDate2 = assigmentSellerDB.dateCreated;

              var dateToSeconds2 = inicialDate2.getTime();

              var nameDist2 = assigmentSellerDB.nameAssigning;

              let date5 = date2 - dateToSeconds2;

              // console.log(iccSeller);
              if (iccSeller == iccDist) {
                var distRefer = Assignment.find({ idUserAssign: idDistDirect });
                // console.log('Ganaste puntos por la venta del icc es', iccSeller, 'dist tu id es', idDistDirect);
                if (date5 >= milliseconds) {
                  console.log("Tienes un punto disponible por la venta del icc",iccSeller,"dist tu id es",idDistDirect,
                    "tu nombre",
                    nameDist2
                  );
                // total2.push(nameDist2);
                // // console.log(total2);
                // console.log(total2.length);
                } else if (date5 <= milliseconds) {
                  console.log(
                    "Tienes un punto en espera por la venta del icc",
                    iccSeller,
                    "dist tu id es",
                    idDistDirect,
                    "tu nombre",
                    nameDist2
                  );
                } else {
                  console.log("No puede acceder a asignacion de puntos");
                }
              }
            });
          }
        } else {
          console.log("No calificas para asignación de puntos");
        }
      });
      return response.status(200).send({
        // todos los usuarios distribuidores
        status: true,
        message: "Lista de usuarios distribuidores",
        assignmentUsers: assignmentUsersDB //array de objetos
      });
    } else {
      // console.log('no son distribuidores');
      return response.status(200).send({
        status: false,
        message: "User not found",
        assignmentUserDelete: null
      });
    }
  },


  pointsDistributorWeb: async (request, response) => {
    // console.log(request);
    const parameters = request.body;

    var userBuy = new Buy();
    
    var idDistribuidor = parameters.id;

    var nameDistribuidor = parameters.name;

    var lastnameDistribuidor = parameters.lastname;

    var pointsToProduct = parameters.points;

    var pointsByIcc = parameters.point;

    //variables para guardar en bd
    userBuy.ifPenalty = parameters.penalty; // por default false

    userBuy.descriptionProduct = parameters.description; // como parametro del body

    userBuy.pointsToProduct = parameters.points; // cantidad de puntos a canjear

    userBuy.idDistribuidor = parameters.id;

    userBuy.nameDistributor = parameters.name;

    userBuy.lastnameDistributor = parameters.lastname;
    //

    const assignmentUsers = await Assignment.find({ userType: 'Distribuidor' });

    const assignmentSellers = await Assignment.find({ userType: 'Vendedor' });

    const buyUserSwap = await Buy.find({idDistribuidor: idDistribuidor});

    var seller = []; //variable que almacena la respuesta booleana de usuario vendedor

    var sellerW = []; //variable que almacena la respuesta booleana de usuario vendedor

    var sellerA = []; //variable que almacena la respuesta booleana de usuario vendedor

    var sellerIW = []; //variable que almacena la respuesta booleana de usuario vendedor

    var idDistDirect = '';

    var iccSeller = '';
    
    var iccDist = '';

    var iccSeller2 = '';

    var status = '';

    var totalPenalty = [];

    
    if (assignmentUsers) {
      let assignmentUsersDB = [];

      let assignmentSellersDB = [];

      let dateToday = new Date(); //fecha actual

      // console.log('dateToadte', dateToday);

      let date2 = dateToday.getTime(); // fecha actual en milisegundos

      // console.log('fecha actual a milisegundos', date2);

      var milliseconds = 2592000000; // 30 días en milisegundos

      var date4 = '';

      assignmentSellers.forEach(assignmentSeller => {

        let assigmentSellerDB = assignmentSeller.toObject(); // todos los vendedores

        assignmentSellersDB.push(assigmentSellerDB);

        iccSeller = assigmentSellerDB.iccsim; // icc que han vendido los sellers en general

      });


      assignmentUsers.forEach(assignmentUser => {
        let assigmentUserDB = assignmentUser.toObject(); // todos los distribuidores

        assignmentUsersDB.push(assigmentUserDB);

        let inicialDate = assigmentUserDB.dateCreated; // fecha de venta icc por parte de un distribuidor

        let dateToSeconds = inicialDate.getTime(); // fecha asignacion a milisegundos

        idDistDirect = assigmentUserDB.idUserAssign; // id de los distribuidores

        status = assigmentUserDB.status; // todos los estatus

        // console.log(status);

        date4 = date2 - dateToSeconds;

        var userSeller = assigmentUserDB.ifAssigningSeller;

        var finalUser = assigmentUserDB.ifAssignedFinalUser;

        
        iccDist = assigmentUserDB.iccsim; //todos los iccs que han asigando los vendedores, se filtra al final segun el id del vendedor

        if (date4 >= milliseconds && userSeller == false && idDistDirect == idDistribuidor) {// evaluo todas las icc que estan disponibles x equis dist, si quisieramos que fuera icc disp en general quito el && idDist == idDistribuior
          seller.push(idDistDirect); //cant icc disponibles en para user dist en particular
          if (status == 'portado') {
            totalPenalty.push(idDistDirect);
          }
          
        }else if (date4 <= milliseconds && userSeller == false && idDistDirect == idDistribuidor) {//evaluo icc en espera entre distribuidores
          sellerW.push(idDistDirect);
          if (status == 'portado') {
            totalPenalty.push(idDistDirect);
          }

        }else if (date4 >= milliseconds && userSeller == true && idDistDirect == idDistribuidor) { //evaluo icc disponibles para un dist por parte del vendedor
            sellerA.push(idDistDirect);
            if (status == 'portado') {
              totalPenalty.push(idDistDirect);
            }
            // console.log('icc disponible es', iccSeller);
          
        }else if (date4 <= milliseconds && userSeller == true && finalUser == false && idDistDirect == idDistribuidor) {
          sellerIW.push(idDistDirect);
          if (status == 'portado') {
            totalPenalty.push(idDistDirect);
          }
          // console.log('wait', sellerIW, 'con icc', iccDist);
        }
      });


           //muestra de resultados de puntos por pantalla

          console.log('cant icc disponibles directos para el usuario' ,idDistribuidor, 'es de', seller.length);
          console.log('cant icc en espera directos para el usuario' ,idDistribuidor, 'es de', sellerW.length);
          console.log('cant icc disponibles para distribuidor', idDistribuidor, 'por parte del vendedor es de', sellerA.length);
          console.log('cant icc en espera para distribuidor', idDistribuidor, 'por parte del vendedor es de', sellerIW.length);

      //calculo del canje

      if (!buyUserSwap) { //inicio proceso de canje
        console.log('usuario no existe');
    }else {        
      
      // var totalPointsA = (seller.length + sellerA.length) * pointsByIcc; // total puntos disponibles

      var totalPointsA = '';

        totalPointsA = (seller.length + sellerA.length) * pointsByIcc; // total puntos disponibles

        if (totalPenalty.length > 0) {
          var totalPointsA = totalPointsA - (totalPenalty.length * pointsByIcc);
          // console.log('quitar pts', totalPointsA);
          // console.log('el total de puntos disponibles es', totalPointsA);

        } else {
          
          // console.log('el total de puntos disponibles es', totalPointsA);
          totalPointsA = (seller.length + sellerA.length) * pointsByIcc

        }

        var realTotal = (seller.length + sellerA.length) * pointsByIcc;
        console.log('el total de puntos disponibles es', realTotal, 'por penalizacion descontamos', totalPenalty.length* pointsByIcc, 'tienes disponibles:', totalPointsA);
        console.log('el total de ICCS en espera es', sellerIW.length + sellerW.length) ;

        
        if (totalPointsA >= pointsToProduct) {
  
        totalPointsA = totalPointsA - pointsToProduct;
        console.log('canjeaste', pointsToProduct, 'restan', totalPointsA);
      } else {
        console.log('No tienes suficientes puntos disponibles');
      }

    }

    }else{
      console.log('usuario distribuidor no existe');
    }

    const userBuyAux = await Buy.findOne({idDistribuidor: userBuy.idDistribuidor});

    if (userBuyAux || !userBuyAux) {
      // const UserBuyStored = await userBuy.save();

      return response.status(200).send({
          status: true,
          message: 'Registered buy successfully',
          UserBuyStored: UserBuyStored
      });

  } else {
      return response.status(200).send({
          status: false,
          message: 'Something went wrong with register',
      });
  }
}


};

module.exports = controller;
