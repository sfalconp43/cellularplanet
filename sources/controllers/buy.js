"use strict";

const Buy = require("../models/buy");
const Assignment = require("../models/assignment");
const bcrypt = require("bcrypt");
const config = require("../config");

var controller = {
  test: (request, response) => {
    console.log(request.body);
    return response.status(200).send({
      message: "Whole Saler controller test"
    });
  },

  saveBuyWeb: async (request, response) => {
    // console.log(request);
    const parameters = request.body;

    var userBuy = new Buy();

    var idDistribuidor = parameters.id;

    var nameDistribuidor = parameters.name;

    var lastnameDistribuidor = parameters.lastname;

    var pointsToProduct = parameters.points;

    var pointsByIcc = parameters.point;

    //variables para guardar en bd
    userBuy.ifPenalty = parameters.penalty; // por default false

    userBuy.descriptionProduct = parameters.description; // como parametro del body

    userBuy.pointsToProduct = parameters.points; // cantidad de puntos a canjear

    userBuy.idDistribuidor = parameters.id;

    userBuy.nameDistributor = parameters.name;

    userBuy.lastnameDistributor = parameters.lastname;
    //

    const assignmentUsers = await Assignment.find({ userType: "Distribuidor" });

    const assignmentSellers = await Assignment.find({ userType: "Vendedor" });

    const buyUserSwap = await Buy.find({ idDistribuidor: idDistribuidor });

    var seller = []; //variable que almacena la respuesta booleana de usuario vendedor

    var sellerW = []; //variable que almacena la respuesta booleana de usuario vendedor

    var sellerA = []; //variable que almacena la respuesta booleana de usuario vendedor

    var sellerIW = []; //variable que almacena la respuesta booleana de usuario vendedor

    var idDistDirect = "";

    var iccSeller = "";

    var iccDist = "";

    var iccSeller2 = "";

    var status = "";

    var totalPenalty = []; //array para guardar penalizaciones

    if (assignmentUsers) {
      let assignmentUsersDB = [];

      let assignmentSellersDB = [];

      let dateToday = new Date(); //fecha actual

      // console.log('dateToadte', dateToday);

      let date2 = dateToday.getTime(); // fecha actual en milisegundos

      // console.log('fecha actual a milisegundos', date2);

      var milliseconds = 2592000000; // 30 días en milisegundos

      var date4 = "";

      assignmentSellers.forEach(assignmentSeller => {
        let assigmentSellerDB = assignmentSeller.toObject(); // todos los vendedores

        assignmentSellersDB.push(assigmentSellerDB);

        iccSeller = assigmentSellerDB.iccsim; // icc que han vendido los sellers en general
      });

      assignmentUsers.forEach(assignmentUser => {
        let assigmentUserDB = assignmentUser.toObject(); // todos los distribuidores

        assignmentUsersDB.push(assigmentUserDB);

        let inicialDate = assigmentUserDB.dateCreated; // fecha de venta icc por parte de un distribuidor

        let dateToSeconds = inicialDate.getTime(); // fecha asignacion a milisegundos

        idDistDirect = assigmentUserDB.idUserAssign; // id de los distribuidores

        status = assigmentUserDB.status; // todos los estatus

        // console.log(status);

        date4 = date2 - dateToSeconds;

        var userSeller = assigmentUserDB.ifAssigningSeller;

        var finalUser = assigmentUserDB.ifAssignedFinalUser;

        iccDist = assigmentUserDB.iccsim; //todos los iccs que han asigando los vendedores, se filtra al final segun el id del vendedor

        if (
          date4 >= milliseconds &&
          userSeller == false &&
          idDistDirect == idDistribuidor
        ) {
          // evaluo todas las icc que estan disponibles x equis dist, si quisieramos que fuera icc disp en general quito el && idDist == idDistribuior
          seller.push(idDistDirect); //cant icc disponibles en para user dist en particular
          if (status == "portado") {
            totalPenalty.push(idDistDirect);
          }
        } else if (
          date4 <= milliseconds &&
          userSeller == false &&
          idDistDirect == idDistribuidor
        ) {
          //evaluo icc en espera entre distribuidores
          sellerW.push(idDistDirect);
          if (status == "portado") {
            totalPenalty.push(idDistDirect);
          }
        } else if (
          date4 >= milliseconds &&
          userSeller == true &&
          idDistDirect == idDistribuidor
        ) {
          //evaluo icc disponibles para un dist por parte del vendedor
          sellerA.push(idDistDirect);
          if (status == "portado") {
            totalPenalty.push(idDistDirect);
          }
          // console.log('icc disponible es', iccSeller);
        } else if (
          date4 <= milliseconds &&
          userSeller == true &&
          finalUser == false &&
          idDistDirect == idDistribuidor
        ) {
          sellerIW.push(idDistDirect);
          if (status == "portado") {
            totalPenalty.push(idDistDirect);
          }
          // console.log('wait', sellerIW, 'con icc', iccDist);
        }
      });

      //muestra de resultados de puntos por pantalla

      console.log(
        "cant icc disponibles directos para el usuario",
        idDistribuidor,
        "es de",
        seller.length
      );
      console.log(
        "cant icc en espera directos para el usuario",
        idDistribuidor,
        "es de",
        sellerW.length
      );
      console.log(
        "cant icc disponibles para distribuidor",
        idDistribuidor,
        "por parte del vendedor es de",
        sellerA.length
      );
      console.log(
        "cant icc en espera para distribuidor",
        idDistribuidor,
        "por parte del vendedor es de",
        sellerIW.length
      );

      //calculo del canje

      if (!buyUserSwap) {
        //inicio proceso de canje
        console.log("usuario no existe");
      } else {
        // var totalPointsA = (seller.length + sellerA.length) * pointsByIcc; // total puntos disponibles

        var totalPointsA = "";

        totalPointsA = (seller.length + sellerA.length) * pointsByIcc; // total puntos disponibles

        if (totalPenalty.length > 0) {
          var totalPointsA = totalPointsA - totalPenalty.length * pointsByIcc;
          // console.log('quitar pts', totalPointsA);
          // console.log('el total de puntos disponibles es', totalPointsA);
        } else {
          // console.log('el total de puntos disponibles es', totalPointsA);
          totalPointsA = (seller.length + sellerA.length) * pointsByIcc;
        }
        
        var realTotal = (seller.length + sellerA.length) * pointsByIcc;
        console.log(
          "el total de puntos disponibles es",
          realTotal,
          "por penalizacion descontamos",
          totalPenalty.length * pointsByIcc,
          "tienes disponibles:",
          totalPointsA
        );
        console.log(
          "el total de ICCS en espera es",
          sellerIW.length + sellerW.length
        );

        if (totalPointsA >= pointsToProduct) {
          totalPointsA = totalPointsA - pointsToProduct;
          console.log("canjeaste", pointsToProduct, "restan", totalPointsA);

          const userBuyAux = await Buy.findOne({
            idDistribuidor: userBuy.idDistribuidor
          });
          if (userBuyAux || !userBuyAux) {
            const UserBuyStored = await userBuy.save();
      
            return response.status(200).send({
              status: true,
              message: "Registered buy successfully",
              UserBuyStored: UserBuyStored
            });
          } else {
            return response.status(200).send({
              status: false,
              message: "Something went wrong with register"
            });
          }
        } else {
          console.log("No tienes suficientes puntos disponibles");
          return response.status(200).send({
            status: false,
            message: "No cuenta con suficientes puntos"
          });
        }
      }
    } else {
      console.log("usuario distribuidor no existe");
    }

    // const userBuyAux = await Buy.findOne({
    //   idDistribuidor: userBuy.idDistribuidor
    // });

    // if (userBuyAux || !userBuyAux) {
    //   const UserBuyStored = await userBuy.save();

    //   return response.status(200).send({
    //     status: true,
    //     message: "Registered buy successfully",
    //     UserBuyStored: UserBuyStored
    //   });
    // } else {
    //   return response.status(200).send({
    //     status: false,
    //     message: "Something went wrong with register"
    //   });
    // }
  },

  updateBuy: async (request, response) => {
    let update = {};
    const parameters = request.body;
    const buyUserId = parameters.id;

    if (parameters.points) {
      update.pointsQuantity = parameters.points;
    }

    const buyUserUpdate = await Buy.findByIdAndUpdate(buyUserId, update, {
      new: true
    });

    if (!buyUserUpdate) {
      return response.status(200).send({
        status: false,
        message: "User not found",
        sellerUserUpdate: null
      });
    } else {
      return response.status(200).send({
        status: true,
        message: "Updated user",
        buyUserUpdate: buyUserUpdate
      });
    }
  },

  getBuyUser: async (request, response) => {
    const parameters = request.body;
    var buyUserId = parameters.id;

    const userBuy = await Buy.findById(buyUserId);

    if (!userBuy) {
      return response.status(200).send({
        status: false,
        message: "User not found"
      });
    } else {
      let BuyDB = userBuy.toObject();

      return response.status(200).send({
        status: true,
        message: "User found",
        userBuy: BuyDB
      });
    }
  },

  getBuyUsers: async (request, response) => {
    const buyUsers = await Buy.find({});

    let buyUsersDB = [];

    buyUsers.forEach(buyUser => {
      let buyUserDB = buyUser.toObject();
      let buyUser2 = buyUserDB;

      buyUsersDB.push(buyUser2);

      console.log(buyUserDB);
    });

    return response.status(200).send({
      status: true,
      message: "list of buyers",
      buyUsers: buyUsersDB
    });
  },

  deleteBuyUser: async (request, response) => {
    const parameters = request.body;
    const buyUserId = parameters.id;

    const buyUserDelete = await Buy.findByIdAndRemove(buyUserId);
    distArray.push(idDistDirect);

    console.log("arrays id de cada distribuidor", distArray); //array de ids usuarios distribuidores general

    console.log(
      "arrays id de cada distribuidor con icc disponibles",
      availableArray
    ); //array de ids usuarios distribuidores icc disponibles

    // console.log('Distribuidores',distributorAll);
    console.log("Cant total de asiganciones de users Distr", seller.length); //cant de icc directas del distribuido

    if (!buyUserDelete) {
      return response.status(200).send({
        status: false,
        message: "User not found",
        buyUserDelete: null
      });
    } else {
      return response.status(200).send({
        status: true,
        message: "deleted user",
        buyUserDelete: buyUserDelete
      });
    }
  }
};

module.exports = controller;
