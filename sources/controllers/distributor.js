'use strict'

const SellerUser = require('../models/sellerUser');
const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Whole Saler controller test'
        });
    },

    saveSellerUser: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var sellerUser = new SellerUser();

        sellerUser.name = parameters.name;
        sellerUser.lastname = parameters.lastname;
        sellerUser.email = parameters.email;
        sellerUser.ifActive = parameters.ifActive;
        sellerUser.idOfCreator = parameters.idOfCreator;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        sellerUser.password = passwordEscripted;
        const sellerUserAux = await SellerUser.findOne({ email: sellerUser.email });

        if (!sellerUserAux) {
            const sellerUserStored = await sellerUser.save();

            return response.status(200).send({
                status: true,
                message: 'Registered user successfully',
                sellerUserStored: sellerUserStored
            });

        } else {
            return response.status(200).send({
                status: false,
                message: 'User already registered',
            });
        }
    },

    saveSellerUserWeb: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var sellerUser = new SellerUser();

        sellerUser.name = parameters.name;
        sellerUser.lastname = parameters.lastname;
        sellerUser.email = parameters.email;
        sellerUser.ifActive = parameters.ifActive;
        sellerUser.idOfCreator = parameters.idOfCreator;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        sellerUser.password = passwordEscripted;
        const sellerUserAux = await SellerUser.findOne({ email: sellerUser.email });

        if (!sellerUserAux) {
            const sellerUserStored = await sellerUser.save();

            request.flash('success-message', 'Vendedor guardado de forma exitosa');
            return response.status(200).redirect('usuarios-vendedores-por-distribuidor');

        } else {
            request.flash('error-message', 'Error al guardar el vendedor. El correo proporcionado ya se encuentra en uso por otro vendedor.');
            return response.status(200).redirect('usuarios-vendedores-por-distribuidor');
        }
    },

    updateSeller: async (request, response) => {
        let update = {};
        const parameters = request.body
        const sellerUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }

        if (parameters.email) {
            update.ifActive = parameters.ifActive;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const sellerUserUpdate = await SellerUser.findByIdAndUpdate(sellerUserId, update, { new: true });

        if (!sellerUserUpdate) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                sellerUserUpdate: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'Updated user',
                sellerUserUpdate: sellerUserUpdate
            });
        }
    },

    updateSellerWeb: async (request, response) => {
        let update = {};
        const parameters = request.body
        const sellerUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }

        if (parameters.email) {
            update.ifActive = parameters.ifActive;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const sellerUserUpdate = await SellerUser.findByIdAndUpdate(sellerUserId, update, { new: true });

        if (!sellerUserUpdate) {
            request.flash('error-message', 'Error al actualizar el vendedor.');
            return response.status(200).redirect('usuarios-vendedores-por-distribuidor');
        } else {
            request.flash('success-message', 'Vendedor actualizado de forma exitosa');
            return response.status(200).redirect('usuarios-vendedores-por-distribuidor');
        }
    },

    getSellerUser: async (request, response) => {
        const parameters = request.body
        var sellerUserId = parameters.id;

        const sellerUser = await SellerUser.findById(sellerUserId);

        if (!sellerUser) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
            });
        } else {
            let SellerDB = sellerUser.toObject();
            delete SellerDB.password;

            return response.status(200).send({
                status: true,
                message: 'User found',
                sellerUser: SellerDB
            });
        }
    },

    getSellerUsers: async (request, response) => {
        const sellerUsers = await SellerUser.find({});

        let sellerUsersDB = [];

        sellerUsers.forEach(sellerUsers => {
            let sellerUsersDB = sellerUser.toObject();
            delete sellerUserDB.password;
            sellerUsersDB.push(sellerUserDB);
        });

        return response.status(200).send({
            status: true,
            message: 'list of sallers',
            sellerUsers: sellerUserDB
        });
    },

    deleteSellerUser: async (request, response) => {
        const parameters = request.body;
        const sellerUserId = parameters.id;

        const sellerUserDelete = await SellerUser.findByIdAndRemove(sellerUserId);

        if (!sellerUserDelete) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                sellerUserDelete: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'deleted user',
                sellerUserDelete: sellerUserDelete
            });
        }
    },

    deleteSellerUserWeb: async (request, response) => {
        const parameters = request.body;
        const sellerUserId = parameters.id;

        const sellerUserDelete = await SellerUser.findByIdAndRemove(sellerUserId);

        if (!sellerUserDelete) {
            request.flash('error-message', 'Error al borrar el vendedor seleccionado');
            return response.status(200).redirect('usuarios-vendedores-por-distribuidor');
        } else {
            request.flash('success-message', 'Distribuidor eliminado de forma exitosa');
            return response.status(200).redirect('usuarios-vendedores-por-distribuidor');
        }
    }

    
};

module.exports = controller;