'use strict'

const WholeSaler = require('../models/wholeSaler');
const Distributor = require('../models/distributor');
const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Whole Saler controller test'
        });
    },

    saveWholeSaler: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var wholeSaler = new WholeSaler();

        wholeSaler.name = parameters.name;
        wholeSaler.lastname = parameters.lastname;
        wholeSaler.email = parameters.email;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        wholeSaler.password = passwordEscripted;
        const wholeSalerAux = await WholeSaler.findOne({ email: wholeSaler.email });
        wholeSaler.ifPrincipal = parameters.ifPrincipal;

        if (!wholeSalerAux) {
            const wholeSalerStored = await wholeSaler.save();

            return response.status(200).send({
                status: true,
                message: 'Registered user successfully',
                wholeSalerStored: wholeSalerStored
            });

        } else {
            return response.status(200).send({
                status: false,
                message: 'User already registered',
            });
        }
    },

    saveWholeSalerWeb: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var wholeSaler = new WholeSaler();

        wholeSaler.name = parameters.name;
        wholeSaler.lastname = parameters.lastname;
        wholeSaler.email = parameters.email;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        wholeSaler.password = passwordEscripted;
        const wholeSalerAux = await WholeSaler.findOne({ email: wholeSaler.email });
        wholeSaler.ifPrincipal = parameters.ifPrincipal;

        if (!wholeSalerAux) {
            const wholeSalerStored = await wholeSaler.save();


            request.flash('success-message', 'Mayorista guardado de forma exitosa');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');

            // return response.status(200).send({
            //     status: true,
            //     message: 'Registered user successfully',
            //     wholeSalerStored: wholeSalerStored
            // });

        } else {
            request.flash('error-message', 'Error al guardar el mayorista');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: false,
            //     message: 'User already registered',
            // });
        }
    },

    updateWholeSaler: async (request, response) => {
        let update = {};
        const parameters = request.body
        const wholeSalerUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const whoSalerUpdate = await WholeSaler.findByIdAndUpdate(wholeSalerUserId, update, { new: true });

        if (!whoSalerUpdate) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                whoSalerUpdate: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'Updated user',
                whoSalerUpdateter: whoSalerUpdate
            });
        }
    },

    updateWholeSalerWeb: async (request, response) => {
        let update = {};
        const parameters = request.body
        const wholeSalerUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const whoSalerUpdate = await WholeSaler.findByIdAndUpdate(wholeSalerUserId, update, { new: true });

        if (!whoSalerUpdate) {
            request.flash('error-message', 'Error al actualizar el mayorista');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: false,
            //     message: 'User not found',
            //     whoSalerUpdate: null
            // });
        } else {
            request.flash('success-message', 'Mayorista actualizado de forma exitosa');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: true,
            //     message: 'Updated user',
            //     whoSalerUpdateter: whoSalerUpdate
            // });
        }
    },

    updateUserDistributor: async (request, response) => {
        let update = {};
        const parameters = request.body
        const distributorUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }
        if (parameters.points) {
            update.points = parameters.points;
        }

        if (parameters.points) {
            update.ifActive = parameters.ifActive;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const distributorUserUpdate = await Distributor.findByIdAndUpdate(distributorUserId, update, { new: true });

        if (!distributorUserUpdate) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                whoSalerUpdate: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'Updated user',
                distributorUserUpdate: distributorUserUpdate
            });
        }
    },

    updateUserDistributorWeb: async (request, response) => {
        let update = {};
        const parameters = request.body
        const distributorUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }
        if (parameters.points) {
            update.points = parameters.points;
        }

        if (parameters.points) {
            update.ifActive = parameters.ifActive;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const distributorUserUpdate = await Distributor.findByIdAndUpdate(distributorUserId, update, { new: true });

        if (!distributorUserUpdate) {
            request.flash('error-message', 'Error al actualizar el distribuidor.');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');
        } else {
            request.flash('success-message', 'Distribuidor actualizado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');
        }
    },

    updateUserDistributorWebTwo: async (request, response) => {
        let update = {};
        const parameters = request.body
        const distributorUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }
        if (parameters.points) {
            update.points = parameters.points;
        }

        if (parameters.points) {
            update.ifActive = parameters.ifActive;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const distributorUserUpdate = await Distributor.findByIdAndUpdate(distributorUserId, update, { new: true });

        if (!distributorUserUpdate) {
            request.flash('error-message', 'Error al actualizar el distribuidor.');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-secundario');
        } else {
            request.flash('success-message', 'Distribuidor actualizado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-secundario');
        }
    },

    getWholeSalerUser: async (request, response) => {
        const parameters = request.body
        var wholeSalerUserId = parameters.id;

        const wholeSaleUser = await WholeSaler.findById(wholeSalerUserId);

        if (!wholeSaleUser) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
            });
        } else {
            let wholeSalerDB = wholeSaleUser.toObject();
            delete wholeSalerDB.password;

            return response.status(200).send({
                status: true,
                message: 'User found',
                wholeSaleUser: wholeSalerDB
            });
        }
    },

    getWholeSalerUsers: async (request, response) => {
        const salerUsers = await WholeSaler.find({});

        let salerUsersDB = [];

        salerUsers.forEach(salerUser => {
            let salerUserDB = salerUser.toObject();
            delete salerUserDB.password;
            salerUsersDB.push(salerUserDB);
        });

        return response.status(200).send({
            status: true,
            message: 'list of whole salers',
            salerUsers: salerUsersDB
        });
    },

    deleteWholeSalerUser: async (request, response) => {
        const parameters = request.body;
        const wholeSalerUserId = parameters.id;

        const wholeSalerUserDelete = await WholeSaler.findByIdAndRemove(wholeSalerUserId);

        if (!wholeSalerUserDelete) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                wholeSalerUserDelete: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'deleted user',
                wholeSalerUserDelete: wholeSalerUserDelete
            });
        }
    },

    deleteWholeSalerUserWeb: async (request, response) => {
        const parameters = request.body;
        const wholeSalerUserId = parameters.id;

        const wholeSalerUserDelete = await WholeSaler.findByIdAndRemove(wholeSalerUserId);

        if (!wholeSalerUserDelete) {
            request.flash('error-message', 'Error al borrar el mayorista seleccionado');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
        } else {
            request.flash('success-message', 'Mayorista eliminado de forma exitosa');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
        }
    },

    saveDistributor: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var distributor = new Distributor();

        distributor.name = parameters.name;
        distributor.lastname = parameters.lastname;
        distributor.email = parameters.email;
        distributor.ifActive = parameters.ifActive;
        // distributor.points = parameters.points;
        distributor.idOfCreator = parameters.idOfCreator;



        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        distributor.password = passwordEscripted;
        const distributorAux = await Distributor.findOne({ email: distributor.email });

        if (!distributorAux) {
            const distributorStored = await distributor.save();

            return response.status(200).send({
                status: true,
                message: 'Registered user successfully',
                wholeSalerStored: distributorStored
            });

        } else {
            return response.status(200).send({
                status: false,
                message: 'User already registered',
            });
        }
    },

    saveDistributorWeb: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var distributor = new Distributor();

        distributor.name = parameters.name;
        distributor.lastname = parameters.lastname;
        distributor.email = parameters.email;
        distributor.ifActive = parameters.ifActive;
        // distributor.points = parameters.points;
        distributor.idOfCreator = parameters.idOfCreator;



        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        distributor.password = passwordEscripted;
        const distributorAux = await Distributor.findOne({ email: distributor.email });

        if (!distributorAux) {
            const distributorStored = await distributor.save();

            request.flash('success-message', 'Distribuidor guardado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');

        } else {
            request.flash('error-message', 'Error al guardar el distribuidor. El correo proporcionado ya se encuentra en uso por otro distribuidor.');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');
        }
    },

    saveDistributorWeb: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var distributor = new Distributor();

        distributor.name = parameters.name;
        distributor.lastname = parameters.lastname;
        distributor.email = parameters.email;
        distributor.ifActive = parameters.ifActive;
        // distributor.points = parameters.points;
        distributor.idOfCreator = parameters.idOfCreator;



        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        distributor.password = passwordEscripted;
        const distributorAux = await Distributor.findOne({ email: distributor.email });

        if (!distributorAux) {
            const distributorStored = await distributor.save();

            request.flash('success-message', 'Distribuidor guardado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');

        } else {
            request.flash('error-message', 'Error al guardar el distribuidor. El correo proporcionado ya se encuentra en uso por otro distribuidor.');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');
        }
    },

    saveDistributorWebTwo: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var distributor = new Distributor();

        distributor.name = parameters.name;
        distributor.lastname = parameters.lastname;
        distributor.email = parameters.email;
        distributor.ifActive = parameters.ifActive;
        // distributor.points = parameters.points;
        distributor.idOfCreator = parameters.idOfCreator;



        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        distributor.password = passwordEscripted;
        const distributorAux = await Distributor.findOne({ email: distributor.email });

        if (!distributorAux) {
            const distributorStored = await distributor.save();

            request.flash('success-message', 'Distribuidor guardado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-secundario');

        } else {
            request.flash('error-message', 'Error al guardar el distribuidor. El correo proporcionado ya se encuentra en uso por otro distribuidor.');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-secundario');
        }
    },

    getDistributorUser: async (request, response) => {
        const parameters = request.body
        var distributorUserId = parameters.id;

        const distributorUser = await Distributor.findById(distributorUserId);

        if (!distributorUser) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
            });
        } else {
            let distributorDB = distributorUser.toObject();
            delete distributorDB.password;

            return response.status(200).send({
                status: true,
                message: 'User found',
                distributorUser: distributorDB
            });
        }
    },

    getDistributorsUsers: async (request, response) => {
        const distributorUsers = await Distributor.find({});

        let distributorsUsersDB = [];

        distributorUsers.forEach(distributorUser => {
            let distributorUserDB = distributorUser.toObject();
            delete distributorUserDB.password;
            distributorsUsersDB.push(distributorUserDB);
        });

        return response.status(200).send({
            status: true,
            message: 'list of whole salers',
            distributorUsers: distributorUsers
        });
    },

    deleteDistributorUser: async (request, response) => {
        const parameters = request.body;
        const distributorUserId = parameters.id;

        const distributorUserDelete = await Distributor.findByIdAndRemove(distributorUserId);

        if (!distributorUserDelete) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
                distributorUserDelete: null
            });
        } else {
            return response.status(200).send({
                status: true,
                message: 'deleted user',
                distributorUserDelete: distributorUserDelete
            });
        }
    },

    deleteDistributorUserWeb: async (request, response) => {
        const parameters = request.body;
        const distributorUserId = parameters.id;

        const distributorUserDelete = await Distributor.findByIdAndRemove(distributorUserId);

        if (!distributorUserDelete) {
            request.flash('error-message', 'Error al borrar el distribuidor seleccionado');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');
        } else {
            request.flash('success-message', 'Distribuidor eliminado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-principal');
        }
    },

    deleteDistributorUserWebTwo: async (request, response) => {
        const parameters = request.body;
        const distributorUserId = parameters.id;

        const distributorUserDelete = await Distributor.findByIdAndRemove(distributorUserId);

        if (!distributorUserDelete) {
            request.flash('error-message', 'Error al borrar el distribuidor seleccionado');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-secundario');
        } else {
            request.flash('success-message', 'Distribuidor eliminado de forma exitosa');
            return response.status(200).redirect('usuarios-distribuidor-por-mayorista-secundario');
        }
    },
};

module.exports = controller;