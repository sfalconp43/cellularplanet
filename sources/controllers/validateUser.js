'use strict'

const bcrypt = require('bcrypt');
const config = require('../config');

const WholeSaler = require('../models/wholeSaler');
const SecondarySaler = require('../models/secondaryWholeSaler');
const Distributor = require('../models/distributor');
const Seller = require('../models/sellerUser');


var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Whole Saler controller test'
        });
    },

    match: async (request, response) => {
        const parameters = request.body;
        const emailWholeSaler = parameters.email;
        const emailSecondarySaler = parameters.email;
        const emailDistributor = parameters.email;
        const emailSeller = parameters.email;
        const passwordWholeSaler = parameters.password;
        const passwordSecondarySaler = parameters.password;
        const passwordDistributor = parameters.password;
        const passwordSeller = parameters.password;

        const wholesaler = await WholeSaler.findOne({ email: emailWholeSaler });
        const secondarysaler = await SecondarySaler.findOne({ email: emailSecondarySaler });
        const distributor = await Distributor.findOne({ email: emailDistributor });
        const seller = await Seller.findOne({ email: emailSeller });

        if(wholesaler){

            // console.log('Soy mayorista');
            let user = wholesaler.toObject();
            var typeUser2 = user;
            var typeUser = typeUser2.ifPrincipal;
            delete user.password;
            const verifiedPassword = await bcrypt.compare(passwordWholeSaler, wholesaler.password);

            if (verifiedPassword) {
                if (typeUser) {
                    console.log('Es un Mayorista')
                }
                else {
                    console.log('No es un usuario Mayorista');
                }
                
            } else {
                console.log('ContraseÑa Invalida');
            }

        }

        else if (secondarysaler) {
            // console.log('soy sucursal')
            let user = secondarysaler.toObject();
            var typeUser2 = user;
            var typeUser = typeUser2.ifPrincipal;
            delete user.password;
            const verifiedPassword = await bcrypt.compare(passwordSecondarySaler, secondarysaler.password);

            if (verifiedPassword) {
                if (typeUser) {
                    console.log('Es un Mayorista')
                }
                else {
                    console.log('Es un usuario Sucursal');
                }
                
            } else {
                console.log('ContraseÑa Invalida');
            }

        }

        else if(distributor){
            // console.log('soy distribuidor');
            let user = distributor.toObject();
            var typeUser2 = user;
            var typeUser = typeUser2.ifPrincipal;
            delete user.password;
            const verifiedPassword = await bcrypt.compare(passwordDistributor, distributor.password);

            if (verifiedPassword) {
                if (typeUser) {
                    console.log('Es un Mayorista')
                }
                else {
                    console.log('Es un usuario Distribuidor');
                }
                
            } else {
                console.log('ContraseÑa Invalida');
            }
        }

        else if(seller) {
            // console.log('soy vendedor');
            let user = seller.toObject();
            var typeUser2 = user;
            var typeUser = typeUser2.ifPrincipal;
            delete user.password;
            const verifiedPassword = await bcrypt.compare(passwordSeller, seller.password);

            if (verifiedPassword) {
                if (typeUser) {
                    console.log('Es un Mayorista')
                }
                else {
                    console.log('Es un usuario Vendedor');
                }
                
            } else {
                console.log('ContraseÑa Invalida');
            }
        }

        else {
            console.log('Usuario no registrado')
        }









    }

}

module.exports = controller;