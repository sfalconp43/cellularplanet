'use strict'

const SellerUser = require('../models/sellerUser');
const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Whole Saler controller test'
        });
    },

    saveSellerUser: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var sellerUser = new SellerUser();

        sellerUser.name = parameters.name;
        sellerUser.lastname = parameters.lastname;
        sellerUser.email = parameters.email;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        sellerUser.password = passwordEscripted;
        const sellerUserAux = await SellerUser.findOne({ email: sellerUser.email });

        if (!sellerUserAux) {
            const sellerUserStored = await sellerUser.save();

            return response.status(200).send({
                status: true,
                message: 'Registered user successfully',
                sellerUserStored: sellerUserStored
            });

        } else {
            return response.status(200).send({
                status: false,
                message: 'User already registered',
            });
        }
    }

    
};

module.exports = controller;