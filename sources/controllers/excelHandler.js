"use strict";

const Assignment = require('../models/assignment');

var Excel = require('exceljs');



    let excelHandler = {
        createExcel: (nameFile, nameSheet, columns, rows) => {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet(nameSheet);
            worksheet.columns = columns;
            worksheet.addRows(rows);
    
            workbook.xlsx.writeFile(nameFile).then(() => {
                console.log(`Archivo '${nameFile}' generado con exito!`)
            });
        },
    
        readExcel: async (nameFile, nameColumn, typefile) => {
            var workbook = new Excel.Workbook();
            let ICCList = [];

            workbook.xlsx.readFile(nameFile).then(() => {
            console.log('se leyo el archivo de nombre', nameFile);
                workbook.eachSheet((sheet) => {
                    var result = sheet.getColumn(nameColumn);

                    result.values.forEach( (element) => {
                    ICCList.push(element);
                    });

                ICCList.shift();

                var fileIcc = ICCList;
                let count = 0;
                const size = fileIcc.length;
                console.log('este es el size', size);
                fileIcc.forEach( (element) => {
                    Assignment.findOne({iccsim: element}).then( (match) => {
                        if (!match) {
                            //console.log('No se han encotrado ICC en este archivo')
                        } else {
                            //console.log(match);

                            if (match.length == 0) {
                                console.log('nada')

                            }else {
                                if (typefile == 'Activo') { //comprobar si es activo
                                                                    
                                var test = match;
                                var idUpdate = test.id;
                                let newStatus = 'activo';
                                Assignment.findByIdAndUpdate(idUpdate, {$set:{status:newStatus}}, { new: true }).then((docs)=>{
                                    console.log(docs)
                                }).catch((error)=>{
                                    console.log(error)
                                })
                                    
                                } else {
                                    //otro estado
                                    console.log('soy el otro estado')
                                    var test = match;
                                    var idUpdate = test.id;
                                    let newStatus = 'portado';
                                    Assignment.findByIdAndUpdate(idUpdate, {$set:{status:newStatus}}, { new: true }).then((docs)=>{
                                        console.log(docs)
                                    }).catch((error)=>{
                                        console.log(error)
                                    })
                                }
                            }
                        }
                        //aumento del count
                        count++;
                        // console.log(count)
                        if (count == size) {
                            resolve(count);
                            
                        }
                    }).catch( (error) => {
                        reject(error);
                        // console.log(error)
                    })
                    });
                });                    
            }).catch((error) => {
                console.log(error)
            });
        },

        //segundo excel
        readExcel2: (nameFile, nameColumn, typefile) => {
            return new Promise(function (resolve, reject) {
                var workbook = new Excel.Workbook();
                let ICCList = [];
    
                workbook.xlsx.readFile(nameFile).then(() => {
                console.log('se leyo el archivo de nombre', nameFile);
                    workbook.eachSheet((sheet) => {
                        var result = sheet.getColumn(nameColumn);
    
                        result.values.forEach( (element) => {
                        ICCList.push(element);
                        });
    
                    ICCList.shift();
    
                    var fileIcc = ICCList;
                    let count = 0;
                    const size = fileIcc.length;
                    console.log('este es el size', size);
                    fileIcc.forEach( (element) => {
                        Assignment.findOne({iccsim: element}).then( (match) => {
                            if (!match) {
                                //console.log('No se han encotrado ICC en este archivo')
                            } else {
                                //console.log(match);
    
                                if (match.length == 0) {
                                    console.log('nada')
    
                                }else {
                                    if (typefile == 'Activo') { //comprobar si es activo
                                                                        
                                    var test = match;
                                    var idUpdate = test.id;
                                    let newStatus = 'activo';
                                    Assignment.findByIdAndUpdate(idUpdate, {$set:{status:newStatus}}, { new: true }).then((docs)=>{
                                        console.log(docs)
                                    }).catch((error)=>{
                                        console.log(error)
                                    })
                                        
                                    } else {
                                        //otro estado
                                        console.log('soy el otro estado')
                                        var test = match;
                                        var idUpdate = test.id;
                                        let newStatus = 'portado';
                                        Assignment.findByIdAndUpdate(idUpdate, {$set:{status:newStatus}}, { new: true }).then((docs)=>{
                                            console.log(docs)
                                        }).catch((error)=>{
                                            console.log(error)
                                        })
                                    }
                                }
                            }
                            //aumento del count
                            count++;
                            // console.log(count)
                            if (count == size) {
                                resolve(count);
                                
                            }
                        }).catch( (error) => {
                            reject(error);
                            // console.log(error)
                        })
                        });
                    });                    
                }).catch((error) => {
                    console.log(error)
                });
            })
        }



    }
    
    


module.exports = excelHandler;
