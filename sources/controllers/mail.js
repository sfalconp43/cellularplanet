"user strict";

const nodemailer = require("nodemailer");
const WholeSaler = require('../models/wholeSaler');
const Distributor = require('../models/distributor');
const sellerUser = require('../models/sellerUser');
const SecondaryWholeSaler = require('../models/secondaryWholeSaler');
const bcrypt = require('bcrypt');
const config = require('../config');




let mailSender = {
  sendMail: async (recivers, body) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.ionos.mx",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: "mailtest@hersymac.com", // generated ethereal user
        pass: "Megustanlosgatos123!" // generated ethereal password
      }
    });

    let info = await transporter.sendMail({
      from: '"Sarai :fantasma:" <mailtest@hersymac.com>', // sender address
      to: "sfalconp43@gmail.com", // list of receivers
      subject: "Hello :marca_de_verificación_gruesa:", // Subject line
      text: "Hello world?", // plain text body
      html: "<b>Hello world?</b>" // html body
    });
    console.log("Message sent: %s", info.messageId);
  },

  forgotPassword: async (request, response) => {
    var parameters = request.body;
    var email = parameters.email;
    let idUser = '';

    console.log(parameters);

    const wholeSaler = await WholeSaler.findOne({ email: email });
    const secondaryWholeSaler = await SecondaryWholeSaler.findOne({ email: email });
    const distributor = await Distributor.findOne({ email: email });
    const seller = await sellerUser.findOne({ email: email });

    if (wholeSaler != null) {
      idUser = wholeSaler._id;
    }

    if (secondaryWholeSaler != null) {
      idUser = secondaryWholeSaler._id;
    }

    if (distributor != null) {
      idUser = distributor._id;
    }

    if (seller != null) {
      idUser = seller._id;
    }

    if (wholeSaler === null && secondaryWholeSaler === null && distributor === null && seller === null) {
      request.flash('error-message', 'Usuario no registrado ');
      response.redirect('/login');
    } else {
      console.log('wholeSaler', wholeSaler);
      console.log('secondaryWholeSaler', secondaryWholeSaler);
      console.log('distributor', distributor);
      console.log('seller', seller);

      let transporter = nodemailer.createTransport({
        host: "smtp.ionos.mx",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: 'mailtest@hersymac.com', // generated ethereal user
          pass: 'Megustanlosgatos123!' // generated ethereal password
        },
        tls: {
          rejectUnauthorized: false
        }
      });

      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: '"Equipo de soporte de Cellular Planet" <mailtest@hersymac.com>', // sender address
        to: email, // list of receivers
        subject: "Recuperación de contraseña – Cellular Planet", // Subject line
        text: `Ingresa al siguiente link para cambiar tu contraseña \n http://localhost:5000/change-password/${idUser}`, // plain text body
        html: `Ingresa al siguiente link para cambiar tu contraseña \n http://localhost:5000/change-password/${idUser}` // html body
      });

      console.log(info);
      request.flash('success-message', 'Por favor revisa tu correo electrónico para continuar con el proceso');
      response.redirect('/login');
    }






    // async..await is not allowed in global scope, must use a wrapper
    // async function main() {

    // }

    // main().catch(console.error);
  },

  changePassword: async (request, response) => {

    let parameters = request.body;
    let update = {};
    console.log(parameters);

    if (parameters.password) {
      const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
      update.password = passwordEscripted
    }

    const user = await WholeSaler.findByIdAndUpdate(parameters.id, update, { new: true });
    console.log('user',user);

    const user2 = await SecondaryWholeSaler.findByIdAndUpdate(parameters.id, update, { new: true });
    console.log('user2',user2);

    const user3 = await Distributor.findByIdAndUpdate(parameters.id, update, { new: true });
    console.log('user3',user3);

    const user4 = await sellerUser.findByIdAndUpdate(parameters.id, update, { new: true });
    console.log('user4',user4);

    request.flash('success-message', 'Contraseña cambiada correctamente');
    response.redirect('/login');
  },
};

module.exports = mailSender;
