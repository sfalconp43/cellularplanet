'use strict'

const ICCAssignment = require('../models/ICCAssignment');
const Reward = require('../models/reward');
// const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        return response.status(200).send({
            message: 'ICC assignment controller test'
        });
    },

    assignSecondaryWholesaler: async (request, response) => {
        const parameters = request.body
        var iccAssignment = new ICCAssignment();

        if (parameters.ICC) {
            iccAssignment.ICC = parameters.ICC;
        } else {
            return response.status(200).send({
                status: false,
                message: 'ICC cannot be empty',
            });
        }

        if (parameters.secondaryWholesaler) {
            iccAssignment.secondaryWholesaler = parameters.secondaryWholesaler;
            iccAssignment.secondaryWholesalerDate = new Date();
        } else {
            return response.status(200).send({
                status: false,
                message: 'secondaryWholesaler cannot be empty',
            });
        }

        const iccAssignmentAux = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignmentAux) {

            const iccAssignmentStored = await iccAssignment.save();

            // request.flash('success-message', 'ICC assigned');
            // return response.status(200).redirect('asignacion-a-usuario-sucursal');

            return response.status(200).send({
                status: true,
                message: 'ICC assigned',
                iccAssignmentStored: iccAssignmentStored
            });

        } else {
            // request.flash('error-message', 'ICC already assigned');
            // return response.status(200).redirect('asignacion-a-usuario-sucursal');
    
            return response.status(200).send({
                status: false,
                message: 'ICC already assigned',
            });
        }
    },

    assignSecondaryWholesalerWeb: async (request, response) => {
        const parameters = request.body
        var iccAssignment = new ICCAssignment();

        if (parameters.ICC) {
            iccAssignment.ICC = parameters.ICC;
        } else {
            request.flash('error-message', 'ICC no puede estar vacío');
            return response.status(200).redirect('asignacion-a-usuario-sucursal');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        if (parameters.secondaryWholesaler) {
            iccAssignment.secondaryWholesaler = parameters.secondaryWholesaler;
            iccAssignment.secondaryWholesalerDate = new Date();
        } else {
            request.flash('error-message', 'Usuario Mayorista Sucursal no puede estar vacío');
            return response.status(200).redirect('asignacion-a-usuario-sucursal');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        const iccAssignmentAux = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignmentAux) {

            const iccAssignmentStored = await iccAssignment.save();

            request.flash('success-message', 'ICC asignado');
            return response.status(200).redirect('asignacion-a-usuario-sucursal');

            // return response.status(200).send({
            //     status: true,
            //     message: 'ICC assigned',
            //     iccAssignmentStored: iccAssignmentStored
            // });

        } else {
            request.flash('error-message', 'ICC ya asignado');
            return response.status(200).redirect('asignacion-a-usuario-sucursal');

            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC already assigned',
            // });
        }
    },

    assignSecondaryWholesalerMulti: async (request, response) => {
        const parameters = request.body;
        console.log(parameters);

        // var iccAssignment = new ICCAssignment();

        if (!parameters.secondaryWholesaler) {
            request.flash('error-message', 'Vendedor Sucursal no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-sucursal');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        if (!parameters.ICC) {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-sucursal');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        let ICCsYaAsignados = [];

        let ICCsUnicos = [... new Set(parameters.ICC)]; //inserta un nuevo array de objetos sin reemplazar los existentes
        var ICCsRepetidos = [];
        var aux = [];
        let count = 0

        parameters.ICC.forEach((value, index) => {
            aux = Object.assign([], parameters.ICC); //Copiado de elemento
            // aux.splice(index, 1); //Se elimina el elemnto q se compara
            if (aux.indexOf(value) != -1 && ICCsRepetidos.indexOf(value) == -1) ICCsRepetidos.push(value);
        });

        const iccAssignmentAux = await ICCAssignment.find({ ICC: parameters.ICC });

        console.log('esta es la prueba:');
        console.log(iccAssignmentAux);

        iccAssignmentAux.forEach(element => {
            ICCsYaAsignados.push(element.ICC);
        });

        if (ICCsRepetidos.length > 0) {
            console.log('hay repetidos:');
            console.log(ICCsRepetidos)

            

        }

        if (ICCsYaAsignados.length > 0) {
            console.log('hay ya asignados:');
            console.log(ICCsYaAsignados)
            
        }

        console.log(ICCsUnicos - ICCsYaAsignados);

        console.log('unicos:')
        console.log(ICCsUnicos);


        let ICCsUnicosFinal = [... ICCsUnicos];


        if (ICCsYaAsignados.length > 0) {
            ICCsUnicos.forEach(unico => {
                console.log(`de unicos: ${unico}`);
                ICCsYaAsignados.forEach(yaAsignado => {
                    console.log(`de ya asignados: ${yaAsignado}`);
                    if (unico == yaAsignado) {
                        console.log(`coinciden para: ${unico}`);
                        var i = ICCsUnicosFinal.indexOf(unico);
                        // ICCsUnicosFinal.splice(i, 1);
                    }
                });
            });
        } else {
            ICCsUnicosFinal = ICCsUnicos;
        }

        // console.log(unico, yaAsignado);



        console.log('unicos final:')
        console.log(ICCsUnicosFinal);

        let prueba = [];

        ICCsUnicosFinal.forEach(element => {
            var iccAssignment = new ICCAssignment();
            iccAssignment.ICC = element;
            iccAssignment.secondaryWholesaler = parameters.secondaryWholesaler;
            iccAssignment.secondaryWholesalerDate = new Date();
            prueba.push(iccAssignment);
            // const iccAssignmentStored = await iccAssignment.save();
        });

        if (prueba.length > 0) {
            ICCAssignment.collection.insert(prueba).then(() => {
                console.log('se inserto el bloque');

                request.flash('success-message', `Se insertaron los siguientes ICCSS: ${ICCsUnicosFinal} Ya Asignados: ${ICCsYaAsignados} Repetidos: ${ICCsRepetidos}`);
                return response.status(200).redirect('asignacion-a-usuario-sucursal');

            }).catch((error) => {
                console.log(error);
            });
        } else {
            
        }
    },


    assignSecondaryWholesalerMultiMPDist2: async (request, response) => {
        const parameters = request.body;
        console.log(parameters);

        // var iccAssignment = new ICCAssignment();

        if (!parameters.distributor) {
            request.flash('error-message', 'Distribuidor Sucursal no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        if (!parameters.ICC) {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mpl');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        let ICCsYaAsignados = [];

        let ICCsUnicos = [... new Set(parameters.ICC)]; //inserta un nuevo array de objetos sin reemplazar los existentes
        var ICCsRepetidos = [];
        var aux = [];
        let count = 0

        parameters.ICC.forEach((value, index) => {
            aux = Object.assign([], parameters.ICC); //Copiado de elemento
            aux.splice(index, 1); //Se elimina el elemnto q se compara
            if (aux.indexOf(value) != -1 && ICCsRepetidos.indexOf(value) == -1) ICCsRepetidos.push(value);
        });

        const iccAssignmentAux = await ICCAssignment.find({ ICC: parameters.ICC });

        console.log('esta es la prueba:');
        console.log(iccAssignmentAux);

        iccAssignmentAux.forEach(element => {
            ICCsYaAsignados.push(element.ICC);
        });

        if (ICCsRepetidos.length > 0) {
            console.log('hay repetidos:');
            console.log(ICCsRepetidos)

            

        }

        if (ICCsYaAsignados.length > 0) {
            console.log('hay ya asignados:');
            console.log(ICCsYaAsignados)
            
        }

        console.log(ICCsUnicos - ICCsYaAsignados);

        console.log('unicos:')
        console.log(ICCsUnicos);


        let ICCsUnicosFinal = [... ICCsUnicos];


        if (ICCsYaAsignados.length > 0) {
            ICCsUnicos.forEach(unico => {
                console.log(`de unicos: ${unico}`);
                ICCsYaAsignados.forEach(yaAsignado => {
                    console.log(`de ya asignados: ${yaAsignado}`);
                    if (unico == yaAsignado) {
                        console.log(`coinciden para: ${unico}`);
                        var i = ICCsUnicosFinal.indexOf(unico);
                        ICCsUnicosFinal.splice(i, 1);
                    }
                });
            });
        } else {
            ICCsUnicosFinal = ICCsUnicos;
        }
        if (ICCsRepetidos.length > 0) {
            ICCsUnicos.forEach(unico => {
                console.log(`de unicos: ${unico}`);
                ICCsRepetidos.forEach(yaRepetido => {
                    console.log(`de ya reoitos: ${yaRepetido}`);
                    if (unico == yaRepetido) {
                        console.log(`coinciden para: ${unico}`);
                        var i = ICCsUnicosFinal.indexOf(unico);
                        ICCsUnicosFinal.splice(i, 1);
                    }
                });
            });
        } else {
            ICCsUnicosFinal = ICCsUnicos;
        }

        // console.log(unico, yaAsignado);

        console.log('unicos final:')
        console.log(ICCsUnicosFinal);

        let prueba = [];

        ICCsUnicosFinal.forEach(element => {
            var iccAssignment = new ICCAssignment();
            iccAssignment.ICC = element;
            iccAssignment.distributor = parameters.distributor;
            iccAssignment.distributorDate = new Date();
            prueba.push(iccAssignment);
            // const iccAssignmentStored = await iccAssignment.save();
        });

        if (prueba.length > 0) {
            ICCAssignment.collection.insert(prueba).then(() => {
                console.log('se inserto el bloque');
                
                request.flash('success-message', `Se insertaron los siguientes ICCSS: ${ICCsUnicosFinal} Ya Asignados: ${ICCsYaAsignados} Repetidos: ${ICCsRepetidos}`);
                return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');

            }).catch((error) => {
                console.log(error);
            });
        } else {
            
        }
    },


    assignSecondaryWholesalerMultiMPDist: async (request, response) => {
        const parameters = request.body;
        console.log(parameters);

        // var iccAssignment = new ICCAssignment();

        if (!parameters.distributor) {
            request.flash('error-message', 'Distribuidor Sucursal no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        if (!parameters.ICC) {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mpl');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        let ICCsYaAsignados = [];

        let ICCsUnicos = [... new Set(parameters.ICC)]; //inserta un nuevo array de objetos sin reemplazar los existentes
        var ICCsRepetidos = [];
        var aux = [];
        let count = 0

        parameters.ICC.forEach((value, index) => {
            aux = Object.assign([], parameters.ICC); //Copiado de elemento
            aux.splice(index, 1); //Se elimina el elemnto q se compara
            if (aux.indexOf(value) != -1 && ICCsRepetidos.indexOf(value) == -1) ICCsRepetidos.push(value);
        });

        const iccAssignmentAux = await ICCAssignment.find({ ICC: parameters.ICC });

        console.log('esta es la prueba:');
        console.log(iccAssignmentAux);

        iccAssignmentAux.forEach(element => {
            ICCsYaAsignados.push(element.ICC);
        });

        if (ICCsRepetidos.length > 0) {
            console.log('hay repetidos:');
            console.log(ICCsRepetidos)

            

        }

        if (ICCsYaAsignados.length > 0) {
            console.log('hay ya asignados:');
            console.log(ICCsYaAsignados)
            
        }

        console.log(ICCsUnicos - ICCsYaAsignados);

        console.log('unicos:')
        console.log(ICCsUnicos);


        let ICCsUnicosFinal = [... ICCsUnicos];


        if (ICCsYaAsignados.length > 0) {
            ICCsUnicos.forEach(unico => {
                console.log(`de unicos: ${unico}`);
                ICCsYaAsignados.forEach(yaAsignado => {
                    console.log(`de ya asignados: ${yaAsignado}`);
                    if (unico == yaAsignado) {
                        console.log(`coinciden para: ${unico}`);
                        var i = ICCsUnicosFinal.indexOf(unico);
                        ICCsUnicosFinal.splice(i, 1);
                    }
                });
            });
        } else {
            ICCsUnicosFinal = ICCsUnicos;
        }

        // console.log(unico, yaAsignado);

        console.log('unicos final:')
        console.log(ICCsUnicosFinal);

        let prueba = [];

        ICCsUnicosFinal.forEach(element => {
            var iccAssignment = new ICCAssignment();
            iccAssignment.ICC = element;
            iccAssignment.distributor = parameters.distributor;
            iccAssignment.distributorDate = new Date();
            prueba.push(iccAssignment);
            // const iccAssignmentStored = await iccAssignment.save();
        });

        if (prueba.length > 0) {
            ICCAssignment.collection.insert(prueba).then(() => {
                console.log('se inserto el bloque');
                
                request.flash('success-message', `Se insertaron los siguientes ICCSS: ${ICCsUnicosFinal} Ya Asignados: ${ICCsYaAsignados} Repetidos: ${ICCsRepetidos}`);
                return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');

            }).catch((error) => {
                console.log(error);
            });
        } else {
            
        }
    },




    assignSecondaryWholesalerMultiMSDist: async (request, response) => {
        const parameters = request.body;
        console.log(parameters);

        // var iccAssignment = new ICCAssignment();

        if (!parameters.distributor) {
            request.flash('error-message', 'Distribuidor no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-ms');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        if (!parameters.ICC) {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-ms');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        let ICCsYaAsignados = [];

        let ICCsUnicos = [... new Set(parameters.ICC)]; //inserta un nuevo array de objetos sin reemplazar los existentes
        var ICCsRepetidos = [];
        var aux = [];
        let count = 0

        parameters.ICC.forEach((value, index) => {
            aux = Object.assign([], parameters.ICC); //Copiado de elemento
            aux.splice(index, 1); //Se elimina el elemnto q se compara
            if (aux.indexOf(value) != -1 && ICCsRepetidos.indexOf(value) == -1) ICCsRepetidos.push(value);
        });

        const iccAssignmentAux = await ICCAssignment.find({ ICC: parameters.ICC });

        console.log('esta es la prueba:');
        console.log(iccAssignmentAux);

        iccAssignmentAux.forEach(element => {
            ICCsYaAsignados.push(element.ICC);
        });

        if (ICCsRepetidos.length > 0) {
            console.log('hay repetidos:');
            console.log(ICCsRepetidos)

            

        }

        if (ICCsYaAsignados.length > 0) {
            console.log('hay ya asignados:');
            console.log(ICCsYaAsignados)
            
        }

        console.log(ICCsUnicos - ICCsYaAsignados);

        console.log('unicos:')
        console.log(ICCsUnicos);


        let ICCsUnicosFinal = [... ICCsUnicos];


        if (ICCsYaAsignados.length > 0) {
            ICCsUnicos.forEach(unico => {
                console.log(`de unicos: ${unico}`);
                ICCsYaAsignados.forEach(yaAsignado => {
                    console.log(`de ya asignados: ${yaAsignado}`);
                    if (unico == yaAsignado) {
                        console.log(`coinciden para: ${unico}`);
                        var i = ICCsUnicosFinal.indexOf(unico);
                        // ICCsUnicosFinal.splice(i, 1);
                    }
                });
            });
        } else {
            ICCsUnicosFinal = ICCsUnicos;
        }

        // console.log(unico, yaAsignado);



        console.log('unicos final:')
        console.log(ICCsUnicosFinal);

        let prueba = [];

        ICCsUnicosFinal.forEach(element => {
            var iccAssignment = new ICCAssignment();
            iccAssignment.ICC = element;
            iccAssignment.distributor = parameters.distributor;
            iccAssignment.secondaryWholesaler = parameters.secondaryWholesaler;
            iccAssignment.distributorDate = new Date();
            prueba.push(iccAssignment);
            // const iccAssignmentStored = await iccAssignment.save();
        });

        if (prueba.length > 0) {
            ICCAssignment.collection.insert(prueba).then(() => {
                console.log('se inserto el bloque');
                
                request.flash('success-message', `Se insertaron los siguientes ICCSS: ${ICCsUnicosFinal} Ya Asignados: ${ICCsYaAsignados} Repetidos: ${ICCsRepetidos}`);
                return response.status(200).redirect('asignacion-a-usuario-distribuidor-ms');

            }).catch((error) => {
                console.log(error);
            });
        } else {
            
        }
    },


    assignSecondaryWholesalerMultiDistSell: async (request, response) => {
        const parameters = request.body;
        console.log(parameters);

        // var iccAssignment = new ICCAssignment();

        if (!parameters.seller) {
            request.flash('error-message', 'Vendedor no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        if (!parameters.ICC) {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        let ICCsYaAsignados = [];

        let ICCsAsignadosDist = [];

        let ICCsUnicos = [... new Set(parameters.ICC)]; //inserta un nuevo array de objetos sin reemplazar los existentes
        var ICCsRepetidos = [];
        var aux = [];
        let count = 0

        parameters.ICC.forEach((value, index) => {
            aux = Object.assign([], parameters.ICC); //Copiado de elemento
            aux.splice(index, 1); //Se elimina el elemnto q se compara
            if (aux.indexOf(value) != -1 && ICCsRepetidos.indexOf(value) == -1) ICCsRepetidos.push(value);
        });

        // console.log('ESTE ES', parameters.distributor);


        const iccAssignmentAux = await ICCAssignment.find({ ICC: parameters.ICC});

        // const iccDist = await ICCAssignment.find({ distributor: parameters.distributor });


        console.log('esta es la prueba:');
        console.log(iccAssignmentAux);

        iccAssignmentAux.forEach(element => {
            ICCsYaAsignados.push(element.ICC);
            ICCsAsignadosDist.push(element.distributor);
        });
        
        

        if (ICCsRepetidos.length > 0) {
            console.log('hay repetidos:');
            console.log(ICCsRepetidos)

        
        }

        if (ICCsYaAsignados.length > 0) {
            console.log('hay ya asignados:');
            console.log(ICCsYaAsignados);
            // iccDist.forEach(dist => {
            //     console.log('EL ID DEL DIST', dist.distributor);
            // });
            
        }






        // if (ICCsAsignadosDist.length > 0) {
        //     console.log('hay ya asignados de este otro distribuidor:');
        //     console.log(ICCsAsignadosDist);
        //     // iccDist.forEach(dist => {
        //     //     console.log('EL ID DEL DIST', dist.distributor);
        //     // });
            
        // }

        // console.log(ICCsUnicos - ICCsYaAsignados);

        // console.log('unicos:')
        // console.log(ICCsUnicos);


        // let ICCsUnicosFinal = [... ICCsUnicos];


        // if (ICCsYaAsignados.length > 0) {

        // } else {
        //     ICCsUnicosFinal = ICCsUnicos;
        // }

        // console.log(unico, yaAsignado);



        // console.log('unicos final:')
        // console.log(ICCsUnicosFinal);


        // if (ICCsYaAsignados.length > 0) {
        //     ICCsUnicos.forEach(unico => {
        //         console.log(`NUEVOS: ${unico}`); // el nuevo icc que esta ingresando
        //         ICCsYaAsignados.forEach(yaAsignado => { //los icc ya asignados
        //             if (unico == yaAsignado && ) { // consultar los id
        //             //     console.log(`LOS YA ASIGNADOS QUE NO SON DE ESTE DIST: ${unico}`);
        //             //     var i = ICCsUnicosFinal.indexOf(unico);
        //             //     ICCsUnicosFinal.splice(i, 1);
        //             //     // console.log('ICC UNICO FINAL',ICCsUnicosFinal );
        //             // }
        //         });
        //     });
        // } else {
        // // console.log('no puede asignar un ICC que no le hayan asignado')
        // }


        let prueba = [];

        // // let ICCsUnicosFinal = [... ICCsYaAsignados];


        ICCsUnicos.forEach(element => {

            var iccAssignment = new ICCAssignment();
            iccAssignment.ICC = element;
            iccAssignment.distributor = parameters.distributor;
            iccAssignment.seller = parameters.seller;
            iccAssignment.sellerDate = new Date();
            prueba.push(iccAssignment);
            // const iccAssignmentStored = await iccAssignment.save();
        });

        if (prueba.length > 0) {
            ICCAssignment.collection.insert(prueba).then(() => {
                console.log('se inserto el bloque');
                
                request.flash('success-message', `Se insertaron los siguientes ICCSS: ${ICCsUnicos} Repetidos: ${ICCsRepetidos}`);
                return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');

            }).catch((error) => {
                console.log(error);
            });
        } else {
            
        }
    },


    assignSellerFinalUser: async (request, response) => {
        const parameters = request.body;
        console.log(parameters);

        // var iccAssignment = new ICCAssignment();

        if (!parameters.DN) {
            request.flash('error-message', 'Cliente final no puede estar vacio');
            return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'secondaryWholesaler cannot be empty',
            // });
        }

        if (!parameters.ICC) {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        let ICCsYaAsignados = [];

        let ICCsUnicos = [... new Set(parameters.ICC)]; //inserta un nuevo array de objetos sin reemplazar los existentes
        var ICCsRepetidos = [];
        var aux = [];
        let count = 0

        parameters.ICC.forEach((value, index) => {
            aux = Object.assign([], parameters.ICC); //Copiado de elemento
            aux.splice(index, 1); //Se elimina el elemnto q se compara
            if (aux.indexOf(value) != -1 && ICCsRepetidos.indexOf(value) == -1) ICCsRepetidos.push(value);
        });

        const iccAssignmentAux = await ICCAssignment.find({ ICC: parameters.ICC });

        console.log('esta es la prueba:');
        console.log(iccAssignmentAux);

        iccAssignmentAux.forEach(element => {
            ICCsYaAsignados.push(element.ICC);
        });


        if (ICCsRepetidos.length > 0) {
            console.log('hay repetidos:');
            console.log(ICCsRepetidos)

        
        }

        if (ICCsYaAsignados.length > 0) {
            console.log('hay ya asignados:');
            console.log(ICCsYaAsignados)
            
        }

        // console.log(ICCsUnicos - ICCsYaAsignados);

        // console.log('unicos:')
        // console.log(ICCsUnicos);


        // let ICCsUnicosFinal = [... ICCsUnicos];


        // if (ICCsYaAsignados.length > 0) {

        // } else {
        //     ICCsUnicosFinal = ICCsUnicos;
        // }

        // console.log(unico, yaAsignado);



        // console.log('unicos final:')
        // console.log(ICCsUnicosFinal);


        // if (ICCsYaAsignados.length > 0) {
        //     ICCsUnicos.forEach(unico => {
        //         console.log(`de unicos: ${unico}`);
        //         ICCsYaAsignados.forEach(yaAsignado => {
        //             console.log(`de ya asignados: ${yaAsignado}`);
        //             if (unico == yaAsignado) {
        //                 console.log(`coinciden para: ${unico}`);
        //                 var i = ICCsUnicosFinal.indexOf(unico);
        //                 ICCsUnicosFinal.splice(i, 1);
        //             }
        //         });
        //     });
        // } else {
        //     ICCsUnicosFinal = ICCsUnicos;
        // }


        let prueba = [];

        ICCsUnicos.forEach(element => {
            var iccAssignment = new ICCAssignment();
            iccAssignment.ICC = element;
            iccAssignment.DN = parameters.DN;
            iccAssignment.finalClient = parameters.finalClient;
            iccAssignment.seller = parameters.seller;
            iccAssignment.finalClientDate = new Date();
            prueba.push(iccAssignment);
            // const iccAssignmentStored = await iccAssignment.save();
        });

        if (prueba.length > 0) {
            ICCAssignment.collection.insert(prueba).then(() => {
                console.log('se inserto el bloque');
                
                request.flash('success-message', `Se insertaron los siguientes ICCSS: ${ICCsUnicos} Repetidos: ${ICCsRepetidos}`);
                return response.status(200).redirect('asignacion-usuario-final-por-vendedor');

            }).catch((error) => {
                console.log(error);
            });
        } else {
            
        }
    },


    assignDistributorPrincipal: async (request, response) => {
        const parameters = request.body
        var iccAssignment = new ICCAssignment();

        if (parameters.ICC) {
            iccAssignment.ICC = parameters.ICC;
        } else {
            return response.status(200).send({
                status: false,
                message: 'ICC cannot be empty',
            });
        }

        if (parameters.distributor) {
            iccAssignment.distributor = parameters.distributor;

            iccAssignment.distributorDate = new Date();
        } else {
            return response.status(200).send({
                status: false,
                message: 'distributor cannot be empty',
            });
        }

        const iccAssignmentAux = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignmentAux) {
            const iccAssignmentStored = await iccAssignment.save();

            // request.flash('success-message', 'ICC assigned');
            // return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');

            return response.status(200).send({
                status: true,
                message: 'ICC assigned',
                iccAssignmentStored: iccAssignmentStored
            });

        } else {
            // request.flash('error-message', 'ICC already assigned');
            // return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            return response.status(200).send({
                status: false,
                message: 'ICC already assigned',
            });
        }
    },

    assignDistributorPrincipalWeb: async (request, response) => {
        const parameters = request.body
        var iccAssignment = new ICCAssignment();

        if (parameters.ICC) {
            iccAssignment.ICC = parameters.ICC;
        } else {
            request.flash('error-message', 'ICC no puede estar vacío');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        if (parameters.distributor) {
            iccAssignment.distributor = parameters.distributor;

            iccAssignment.distributorDate = new Date();
        } else {
            request.flash('error-message', 'Distribuidor no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'distributor cannot be empty',
            // });
        }

        const iccAssignmentAux = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignmentAux) {
            const iccAssignmentStored = await iccAssignment.save();

            request.flash('success-message', 'ICC assigned');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');

            // return response.status(200).send({
            //     status: true,
            //     message: 'ICC assigned',
            //     iccAssignmentStored: iccAssignmentStored
            // });

        } else {
            request.flash('error-message', 'ICC already assigned');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC already assigned',
            // });
        }
    },

    assignDistributorSecundary: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.ICC) {

        } else {
            return response.status(200).send({
                status: false,
                message: 'ICC cannot be empty',
            });
        }

        if (parameters.distributor) {
            update.distributor = parameters.distributor;
            update.distributorDate = new Date();
        } else {
            return response.status(200).send({
                status: false,
                message: 'distributor cannot be empty',
            });
        }

        const iccAssignment = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignment) {
            return response.status(200).send({
                status: false,
                message: 'ICC not found',
            });
        } else {
            if (iccAssignment.secondaryWholesaler != null && iccAssignment.secondaryWholesalerDate != null) {
                const iccAssignmentUpdate = await ICCAssignment.findOneAndUpdate({ ICC: parameters.ICC }, update, { new: true });

                return response.status(200).send({
                    status: true,
                    message: 'ICC assigned',
                    iccAssignmentStored: iccAssignmentUpdate
                });
            } else {
                return response.status(200).send({
                    status: false,
                    message: 'ICC found but without assignment permissions',
                });
            }

        }
    },

    assignDistributorSecundaryWeb: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.ICC) {

        } else {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        if (parameters.distributor) {
            update.distributor = parameters.distributor;
            update.distributorDate = new Date();
        } else {
            request.flash('error-message', 'ICC no puede estar vacio');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'distributor cannot be empty',
            // });
        }

        const iccAssignment = await ICCAssignment.findOne({ICC: parameters.ICC});

        if (!iccAssignment) {
            request.flash('error-message', 'ICC no encontrado');
            return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC not found',
            // });
        } else {
            if (iccAssignment.secondaryWholesaler != null && iccAssignment.secondaryWholesalerDate != null) {
                const iccAssignmentUpdate = await ICCAssignment.findOneAndUpdate({ICC: parameters.ICC}, update, { new: true });

                request.flash('success-message', 'ICC asignado');
                return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');

                // return response.status(200).send({
                //     status: true,
                //     message: 'ICC assigned',
                //     iccAssignmentStored: iccAssignmentUpdate
                // });
            } else {
                request.flash('error-message', 'ICC encontrado pero sin permisos de asignacion');
                return response.status(200).redirect('asignacion-a-usuario-distribuidor-mp');
                // return response.status(200).send({
                //     status: false,
                //     message: 'ICC found but without assignment permissions',
                // });   
            }

        }
    },


    assignSeller: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.ICC) {

        } else {
            return response.status(200).send({
                status: false,
                message: 'ICC cannot be empty',
            });
        }

        if (parameters.seller) {
            update.seller = parameters.seller;
            update.sellerDate = new Date();
        } else {
            return response.status(200).send({
                status: false,
                message: 'seller cannot be empty',
            });
        }

        const iccAssignment = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignment) {
            return response.status(200).send({
                status: false,
                message: 'ICC not found',
            });
        } else {
            if (iccAssignment.distributor != null && iccAssignment.distributorDate != null) {
                const iccAssignmentUpdate = await ICCAssignment.findOneAndUpdate({ ICC: parameters.ICC }, update, { new: true });

            //     request.flash('success-message', 'ICC assigned');
            // return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');

                return response.status(200).send({
                    status: true,
                    message: 'ICC assigned',
                    iccAssignmentStored: iccAssignmentUpdate
                });
            } else {

            //     request.flash('error-message', 'ICC already assigned');
            // return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');

                return response.status(200).send({
                    status: false,
                    message: 'ICC found but without assignment permissions',
                });   
            }

        }
    },


    assignSellerWeb: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.ICC) {

        } else {
                request.flash('error-message', 'ICC no puede estar vacío');
                return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        if (parameters.seller) {
            update.seller = parameters.seller;
            update.sellerDate = new Date();
        } else {
                request.flash('error-message', 'Vendedor no puede estar vacio');
                return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'seller cannot be empty',
            // });
        }
        
        const iccAssignment = await ICCAssignment.findOne({ICC: parameters.ICC});

        if (!iccAssignment) {
                request.flash('error-message', 'ICC no encontrado');
                return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC not found',
            // });
        } else {
            if (iccAssignment.distributor != null && iccAssignment.distributorDate != null) {
                const iccAssignmentUpdate = await ICCAssignment.findOneAndUpdate({ICC: parameters.ICC}, update, { new: true });

                request.flash('success-message', 'ICC assigned');
                return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');

                // return response.status(200).send({
                //     status: true,
                //     message: 'ICC assigned',
                //     iccAssignmentStored: iccAssignmentUpdate
                // });
            } else {

            request.flash('error-message', 'ICC already assigned');
            return response.status(200).redirect('asignacion-a-usuario-vendedor-por-distribuidor');

                // return response.status(200).send({
                //     status: false,
                //     message: 'ICC found but without assignment permissions',
                // });   
            }

        }
    },

    assignFinalClient: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.ICC) {
            update.finalClient = true;
            update.finalClientDate = new Date();
        } else {
            return response.status(200).send({
                status: false,
                message: 'ICC cannot be empty',
            });
        }

        if (parameters.DN) {
            update.DN = parameters.DN;
        }

        const iccAssignment = await ICCAssignment.findOne({ ICC: parameters.ICC });

        if (!iccAssignment) {
            return response.status(200).send({
                status: false,
                message: 'ICC not found',
            });
        } else {
            if ((iccAssignment.distributor != null && iccAssignment.distributorDate != null) || (iccAssignment.seller != null && iccAssignment.seller != null)) {
                const iccAssignmentUpdate = await ICCAssignment.findOneAndUpdate({ ICC: parameters.ICC }, update, { new: true });
                var reward = new Reward();

                reward.distributor = iccAssignmentUpdate.distributor;
                reward.rewardPoints = config.ICC_POINT;

                const rewardStored = await reward.save();

                return response.status(200).send({
                    status: true,
                    message: 'ICC assigned',
                    iccAssignmentStored: iccAssignmentUpdate,
                    rewardStored: rewardStored
                });
            } else {
                return response.status(200).send({
                    status: false,
                    message: 'ICC found but without assignment permissions',
                });
            }

        }
    },

    assignFinalClientWeb: async (request, response) => {
        let update = {};
        const parameters = request.body

        if (parameters.ICC) {
            update.finalClient = true;
            update.finalClientDate = new Date();
        } else {
                request.flash('error-message', 'ICC no puede estar vacio');
                return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC cannot be empty',
            // });
        }

        if (parameters.DN) {
            update.DN = parameters.DN;
        }

        const iccAssignment = await ICCAssignment.findOne({ICC: parameters.ICC});

        if (!iccAssignment) {
                request.flash('error-message', 'ICC no enconrado');
                return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
            // return response.status(200).send({
            //     status: false,
            //     message: 'ICC not found',
            // });
        } else {
            if ((iccAssignment.distributor != null && iccAssignment.distributorDate != null) || (iccAssignment.seller != null && iccAssignment.seller != null)) {
                const iccAssignmentUpdate = await ICCAssignment.findOneAndUpdate({ICC: parameters.ICC}, update, { new: true });
                var reward = new Reward();

                reward.distributor = iccAssignmentUpdate.distributor;
                reward.rewardPoints = config.ICC_POINT;

                const rewardStored = await reward.save();

                request.flash('success-message', 'ICC asignado');
                return response.status(200).redirect('asignacion-usuario-final-por-vendedor');

                // return response.status(200).send({
                //     status: true,
                //     message: 'ICC assigned',
                //     iccAssignmentStored: iccAssignmentUpdate,
                //     rewardStored: rewardStored
                // });
            } else {
                request.flash('error-message', 'ICC encontrado pero sin permisos de asignacion');
                return response.status(200).redirect('asignacion-usuario-final-por-vendedor');
                // return response.status(200).send({
                //     status: false,
                //     message: 'ICC found but without assignment permissions',
                // });   
            }

        }
    },

    getICCAssignments: async (request, response) => {
        const iccAssignments = await ICCAssignment.find();
        return response.status(200).send({
            status: true,
            message: 'list of iccAssignment',
            iccAssignments: iccAssignments
        });

    },

    getICCAssignmentsWeb: async (request, response) => {
        const iccAssignments = await ICCAssignment.find();
        return response.status(200).send({
            status: true,
            message: 'list of iccAssignment',
            iccAssignments: iccAssignments
        });

    },

    getICCAssignment: async (request, response) => {
        const parameters = request.body
        const iccAssignment = await ICCAssignment.findOne({ ICC: parameters.ICC }).populate('secondaryWholesaler');

        if (!iccAssignment) {
            return response.status(200).send({
                status: false,
                message: 'ICC not assigned',
            });


        } else {
            return response.status(200).send({
                status: true,
                message: 'ICC found',
                iccAssignment: iccAssignment
            });
        }
    },

    getICCAssignmentWeb: async (request, response) => {
        const parameters = request.body
        const iccAssignment = await ICCAssignment.findOne({ ICC: parameters.ICC }).populate('secondaryWholesaler');

        if (!iccAssignment) {
            return response.status(200).send({
                status: false,
                message: 'ICC not assigned',
            });


        } else {
            return response.status(200).send({
                status: true,
                message: 'ICC found',
                iccAssignment: iccAssignment
            });
        }
    },


    
};

module.exports = controller;