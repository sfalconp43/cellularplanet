'use strict'

const SecondaryWholeSaler = require('../models/secondaryWholeSaler');
const Distributor = require('../models/distributor');
const sellerUser = require('../models/sellerUser');
const Redemption = require('../models/redemption');
const Penalty = require('../models/penalty');
const Reward = require('../models/reward');
// const config = require('../config');


var controller = {

    test: (request, response) => {
        return response.status(200).send({
            message: 'mobileMethods controller test'
        });
    },

    getUsersMP: async (request, response) => {
        // const parameters = request.body
        const parameters = request.query

        if (parameters.id) {
            const secondaryWholeSalers = await SecondaryWholeSaler.find().sort({name: 1});
            const distributors = await Distributor.find({idOfCreator: parameters.id}).sort({name: 1});

            return response.status(200).send({
                status: true,
                message: 'user for MP',
                secondaryWholeSalers: secondaryWholeSalers,
                distributors: distributors
            });
        } else {
            return response.status(200).send({
                status: false,
                message: 'id cannot be empty',
            });
        }
    },

    getUsersMS: async (request, response) => {
        // const parameters = request.body
        const parameters = request.query

        if (parameters.id) {
            const distributors = await Distributor.find({idOfCreator: parameters.id}).sort({name: 1});

            return response.status(200).send({
                status: true,
                message: 'user for MS',
                distributors: distributors
            });
        } else {
            return response.status(200).send({
                status: false,
                message: 'id cannot be empty',
            });
        }
    },

    getUsersD: async (request, response) => {
        // const parameters = request.body
        const parameters = request.query

        if (parameters.id) {
            const sellers = await sellerUser.find({idOfCreator: parameters.id}).sort({name: 1});

            return response.status(200).send({
                status: true,
                message: 'user for D',
                sellers: sellers
            });
        } else {
            return response.status(200).send({
                status: false,
                message: 'id cannot be empty',
            });
        }
    },

    getDistributors: async (request, response) => {
        const distributors = await Distributor.find().sort({name: 1});

        return response.status(200).send({
            status: true,
            message: 'distributors',
            distributors: distributors
        });
    },

    getPoints: async (request, response) => {
        const parameters = request.query;
        let waitingPoints = 0;
        let pointsAvailable = 0;
        let redeemedPoints = 0;
        const mes = 2592000000;
        let rewardsCount = 0;
        let penaltiesCount = 0;
        let redemptionCount = 0;

        if (parameters.id) {
            const distributor = await Distributor.findOne({_id: parameters.id});
            let distributorDB = distributor.toObject();
            delete distributorDB.password;
            const rewards = await Reward.find({distributor: parameters.id});
            const penalties = await Penalty.find({distributor: parameters.id});
            const redemptions = await Redemption.find({distributor: parameters.id, status: 'Aceptada'});

            console.log(rewards);
            console.log(penalties);
            console.log(redemptions);

            rewards.forEach(element => {
                if (new Date().getTime() - element.dateCreated.getTime() >= mes) {
                    pointsAvailable = pointsAvailable + element.rewardPoints;
                } else {
                    waitingPoints = waitingPoints + element.rewardPoints;
                }

                rewardsCount++;

                if (rewardsCount === rewards.length && penaltiesCount === penalties.length && redemptionCount === redemptions.length) {
                    
                    return response.status(200).send({
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    });
                }
            });

            penalties.forEach(element => {
                pointsAvailable = pointsAvailable - element.penaltyPoints;
                penaltiesCount++;

                if (rewardsCount === rewards.length && penaltiesCount === penalties.length && redemptionCount === redemptions.length) {
                    
                    return response.status(200).send({
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    });
                }
            });

            redemptions.forEach(element => {
                redeemedPoints = redeemedPoints + element.redemptionPoints;
                redemptionCount++;

                if (rewardsCount === rewards.length && penaltiesCount === penalties.length && redemptionCount === redemptions.length) {
                    
                    return response.status(200).send({
                        status: true,
                        message: 'distributor points and info',
                        distributor: distributorDB,
                        waitingPoints: waitingPoints,
                        pointsAvailable: pointsAvailable,
                        redeemedPoints: redeemedPoints
                    });
                }
            });

            if (rewards.length === 0 && penalties.length === 0 && redemptions.length === 0) {
                    
                return response.status(200).send({
                    status: true,
                    message: 'distributor points and info',
                    distributor: distributorDB,
                    waitingPoints: waitingPoints,
                    pointsAvailable: pointsAvailable,
                    redeemedPoints: redeemedPoints
                });
            }
        } else {
            return response.status(200).send({
                status: false,
                message: 'id cannot be empty',
            });
        }
    },
};

module.exports = controller;