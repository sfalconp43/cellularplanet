'use strict'

const SecondaryWholeSaler = require('../models/secondaryWholeSaler');
const bcrypt = require('bcrypt');
const config = require('../config');


var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Whole Saler controller test'
        });
    },

    saveSecondaryWholeSaler: async (request, response) => {
        console.log(request);
        const parameters = request.body
        var secondaryWholeSaler = new SecondaryWholeSaler();

        secondaryWholeSaler.name = parameters.name;
        secondaryWholeSaler.lastname = parameters.lastname;
        secondaryWholeSaler.email = parameters.email;

        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
        secondaryWholeSaler.password = passwordEscripted;
        const secondaryWholeSalerAux = await SecondaryWholeSaler.findOne({ email: secondaryWholeSaler.email });
        secondaryWholeSaler.ifPrincipal = parameters.ifPrincipal;

        if (!secondaryWholeSalerAux) {
            const SecondaryWholeSalerStored = await secondaryWholeSaler.save();

            request.flash('success-message', 'Mayorista sucursal guardado de forma exitosa');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');

            // return response.status(200).send({
            //     status: true,
            //     message: 'Registered user successfully',
            //     SecondaryWholeSalerStored: SecondaryWholeSalerStored
            // });

        } else {
            request.flash('error-message', 'Error al guardar el Mayorista sucursal. El correo proporcionado ya se encuentra en uso por otro Mayorista sucursal.');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: false,
            //     message: 'User already registered',
            // });
        }
    },

    updateSecondaryWholeSaler: async (request, response) => {
        let update = {};
        const parameters = request.body
        const secondaryWholeSalerUserId = parameters.id;

        if (parameters.name) {
            update.name = parameters.name;
        }

        if (parameters.lastname) {
            update.lastname = parameters.lastname;
        }

        if (parameters.email) {
            update.email = parameters.email;
        }

        if (parameters.password) {
            const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);
            update.password = passwordEscripted
        }

        const secondaryWholeSaleUpdate = await SecondaryWholeSaler.findByIdAndUpdate(secondaryWholeSalerUserId, update, { new: true });

        if (!secondaryWholeSaleUpdate) {
            request.flash('error-message', 'Error al actualizar el mayorista sucursal.');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: false,
            //     message: 'User not found',
            //     secondaryWholeSaleUpdate: null
            // });
        } else {
            request.flash('success-message', 'Mayorista Sucursal actualizado de forma exitosa');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: true,
            //     message: 'Updated user',
            //     secondaryWholeSaleUpdate: secondaryWholeSaleUpdate
            // });
        }
    },

    getSecondaryWholeSalerUser: async (request, response) => {
        const parameters = request.body
        var secondaryWholeSalerUserId = parameters.id;

        const secondarywholeSaleUser = await SecondaryWholeSaler.findById(secondaryWholeSalerUserId);

        if (!secondarywholeSaleUser) {
            return response.status(200).send({
                status: false,
                message: 'User not found',
            });
        } else {
            let secondaryWholeSalerDB = secondarywholeSaleUser.toObject();
            delete secondaryWholeSalerDB.password;

            return response.status(200).send({
                status: true,
                message: 'User found',
                secondarywholeSaleUser: secondaryWholeSalerDB
            });
        }
    },

    getsecondaryWholeSalerUsers: async (request, response) => {
        const secondaryUsers = await SecondaryWholeSaler.find({});

        let secondaryUsersDB = [];

        secondaryUsers.forEach(secondarySalerUser => {
            let secundarySalerUserDB = secondarySalerUser.toObject();
            delete secundarySalerUserDB.password;
            secondaryUsersDB.push(secundarySalerUserDB);
        });

        return response.status(200).send({
            status: true,
            message: 'list of whole salers',
            secondaryUsers: secondaryUsersDB
        });
    },

    deleteSecondaryWholeSalerUser: async (request, response) => {
        const parameters = request.body;
        const secondaryWholeSalerUserId = parameters.id;

        const secondarySalerUserDelete = await SecondaryWholeSaler.findByIdAndRemove(secondaryWholeSalerUserId);

        if (!secondarySalerUserDelete) {

            request.flash('error-message', 'Error al borrar el mayorista sucursal seleccionado');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: false,
            //     message: 'User not found',
            //     secondarySalerUserDelete: null
            // });
        } else {
            request.flash('success-message', 'Mayorista sucursal eliminado de forma exitosa');
            return response.status(200).redirect('usuarios-mayoristas-secundarios');
            // return response.status(200).send({
            //     status: true,
            //     message: 'deleted user',
            //     secondarySalerUserDelete: secondarySalerUserDelete
            // });
        }
    },


    
};

module.exports = controller;