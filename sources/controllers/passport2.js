const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const WholeSaler = require('../models/wholeSaler');
const Distributor = require('../models/distributor');
const sellerUser = require('../models/sellerUser');


module.exports = (passport) => {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    passport.use('local-login2', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
        async (request, email, password, done) => {
            // const adminUser = await AdminUser.findOne({ email: email });

            const wholeSaler = await WholeSaler.findOne({ email: email });

            if (!wholeSaler) {

                const distributor = await Distributor.findOne({ email: email });

                if (!distributor) {

                    const seller = await sellerUser.findOne({ email: email });

                    if (!seller) {

                        return done(null, false, request.flash('error-message', 'Usuario no registrado'));

                    } else {
                        console.log('Es un vendedor');
                        console.log(seller);
                        let user = seller.toObject();
                        delete user.password;
                        const verifiedPassword = await bcrypt.compare(password, seller.password);

                        if (verifiedPassword) {
                            return done(null, user);
                        } else {
                            return done(null, false, request.flash('error-message', 'Contraseña Incorrecta'));
                        }
                    }
                } else {
                    console.log('Es un distribuidor');
                    console.log(distributor);
                    let user = distributor.toObject();
                    delete user.password;
                    const verifiedPassword = await bcrypt.compare(password, distributor.password);

                    if (verifiedPassword) {
                        return done(null, user);
                    } else {
                        return done(null, false, request.flash('error-message', 'Contraseña Incorrecta'));
                    }
                }
            } else {
                console.log('Es un mayorista');
                console.log(wholeSaler);
                let user = wholeSaler.toObject();
                delete user.password;
                const verifiedPassword = await bcrypt.compare(password, wholeSaler.password);

                if (verifiedPassword) {
                    return done(null, user);
                } else {
                    return done(null, false, request.flash('error-message', 'Contraseña Incorrecta'));
                }
            }

            // if (!adminUser) {
            //     const sellingUser = await SellingUser.findOne({ email: email });

            //     if (!sellingUser) {
            //         return done(null, false, request.flash('error-message', 'Usuario no registrado'));
            //     } else {
            //         let user = sellingUser.toObject();
            //         delete user.password;
            //         const verifiedPassword = await bcrypt.compare(password, sellingUser.password);

            //         if (verifiedPassword) {
            //             return done(null, user);
            //         } else {
            //             return done(null, false, request.flash('error-message', 'Contraseña Incorrecta'));
            //         }
            //     }

            // } else {
            //     let user = adminUser.toObject();
            //     delete user.password;
            //     const verifiedPassword = await bcrypt.compare(password, adminUser.password);

            //     if (verifiedPassword) {
            //         return done(null, user);
            //     } else {
            //         return done(null, false, request.flash('error-message', 'Contraseña Incorrecta'));
            //     }
            // }
        })
    );
}
