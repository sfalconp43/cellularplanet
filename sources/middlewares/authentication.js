// const jwt = require('jsonwebtoken');
const config = require('../config');

verifyAuthentication = (request, response, next) => {
    const user = request.user;
    if (!user) {
        request.flash('error-message', 'Por favor inicie sesión para usar la plataforma');
        return response.status(200).redirect('login');
    } else {
        next();
    }
};
module.exports = {
    verifyAuthentication
}