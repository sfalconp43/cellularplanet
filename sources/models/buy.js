'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {String}  idDistribuidor    This field stores the name of the adminUser
 * @param {String}  nameDistributor    This field stores the name of the adminUser
 * @param {String}  lastnameDistributor    This field stores the name of the adminUser
 * @param {String}  ifPenalty    This field stores the quantity of points available
 * @param {Date}    dateCreated This field stores the Date when adminUser was craeted on the platform
 * @param {Date}    dateUpdated This field stores the Date every time when the adminUser updates something from its information
 * @param {String}  pointsToProduct    This field stores the quantity of points for product
 * @param {String}  descriptionProduct    This field stores the description of the product
//  * //  * @param {String}  pointsQuantity    This field stores the quantity of points available



 */
const CompraSchema = Schema({
    idDistribuidor: { type: String, required: true },
    nameDistributor: { type: String, required: true },
    lastnameDistributor: { type: String, required: true },
    dateCreated: { type: Date, default: new Date() },
    dateUpdated: { type: Date, default: new Date() },
    ifPenalty: { type: Boolean, default: false },
    pointsToProduct: { type: String, required: true },
    descriptionProduct: { type: String },
    // pointsQuantity: { type: String, required: true },



});

module.exports = mongoose.model('Compra', CompraSchema);