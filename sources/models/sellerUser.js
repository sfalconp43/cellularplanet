'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {String}  name    This field stores the name of the adminUser
 * @param {String}  email   This field stores the email adress of the adminUser
 * @param {String}  lastname    This field stores the lastname of the adminUser
 * @param {String}  password    This field stores the encrypted password of the adminUser
 * @param {String}  ifActive    This field stores the status of the icc chip
 * @param {Date}    dateCreated This field stores the Date when adminUser was craeted on the platform
 * @param {Date}    dateUpdated This field stores the Date every time when the adminUser updates something from its information
 */
const sellerUserSchema = Schema({
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    ifActive: { type: Boolean, default: false },
    dateCreated: { type: Date, default: new Date() },
    dateUpdated: { type: Date, default: new Date() },
    idOfCreator: { type: Schema.Types.ObjectId, default: null}
});

module.exports = mongoose.model('sellerUser', sellerUserSchema);