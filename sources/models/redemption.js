'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {Date} dateCreated
 * @param {ObjectId} distributor
 * @param {Number} redemptionPoints
 * @param {String} description
 * @param {String} description
 */
const RedemptionSchema = Schema({
    dateCreated: { type: Date, default: new Date() },
    distributor: { type: Schema.Types.ObjectId, ref: 'Distributor', default: null },
    redemptionPoints: { type: Number, default: 0},
    description: { type: String, default: '' },
    status: { type: String, default: 'En espera' },
});

module.exports = mongoose.model('Redemption', RedemptionSchema);