'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {String}  iccsim    This field stores the name of the adminUser
 * @param {String}  idusersaler   This field stores the email adress of the adminUser
 * @param {String}  iduserdistributor    This field stores the lastname of the adminUser
 * @param {String}  simstatus    This field stores the encrypted password of the adminUser
 * @param {Date}    dateCreated This field stores the Date when adminUser was craeted on the platform
 * @param {Date}    dateUpdated This field stores the Date every time when the adminUser updates something from its information
 */
const WholeSalerDistAssigmentSchema = Schema({
    iccsim: { type: String, required: true },
    idusersaler: { type: String, required: true },
    iduserdistributor: { type: String, required: true },
    simstatus: { type: String, required: true },
    dateCreated: { type: Date, default: new Date() },
    dateUpdated: { type: Date, default: new Date() },
});

module.exports = mongoose.model('WholeSaleDistAssigment', WholeSalerDistAssigmentSchema);