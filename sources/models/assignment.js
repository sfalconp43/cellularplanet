'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription.
 * @param {String}  iccsim    This field stores the name of the adminUser 
 * @param {String}  name    This field stores the name of the adminUser
 * @param {String}  lastname    This field stores the lastname of the adminUser
 * @param {String}  idUserAssign    This field stores the id of the user assigning the icc
 * @param {String}  nameAssigning    This field stores the id of the user assigning the icc
 * @param {String}  lastnameAssigning    This field stores the id of the user assigning the icc
 * @param {String}  nameAssigned    This field stores the id of the user assigning the icc
 * @param {String}  lastnameAssigned    This field stores the id of the user assigning the icc
 * @param {String}  buyerPhone    This field stores the id of the user assigning the icc
 * @param {String}  idUserAssigned    This field stores the id of the user who is assinged the icc
 * @param {String}  ifAssignedSecondaryWholeSaler    This field stores is the user is a WholeSaler
 * @param {String}  ifAssignedDistributor    This field stores is the user is a distributor
 * @param {String}  ifAssignedSeller    This field stores is the user is a distributor
 * @param {String}  ifAssignedFinalUser   This field stores is the user is a Final User
 * @param {String}  ifAssignedMassively   This field stores is the Icc assign is massively
 * @param {String}  status   This field stores the Icc status at the moment os the assignment
 * @param {String}  userType   This field stores the Icc status at the moment os the assignment
 * @param {Date}    dateCreated This field stores the Date when adminUser was craeted on the platform
 * @param {Date}    dateUpdated This field stores the Date every time when the adminUser updates something from its information
 */
const AssignmentSchema = Schema({
    iccsim: { type: String, required: true },
    nameAssigning: { type: String},
    lastnameAssigning: { type: String},
    nameAssigned: { type: String, required: true },
    lastnameAssigned: { type: String, required: true },
    buyerPhone: { type: String, required: true },
    idUserAssign: { type: String, required: true },
    idUserAssigned: { type: String },
    ifAssigingWholeSaler: { type: Boolean, default: false },
    ifAssigingSecondarySaler: { type: Boolean, default: false },
    ifAssignedSecondaryWholeSaler: { type: Boolean, default: false },
    ifAssigningDistributor: { type: Boolean, default: false },
    ifAssigningSeller: { type: Boolean, default: false },
    ifAssignedDistributor: { type: Boolean, default: false },
    ifAssignedSeller: { type: Boolean, default: false },
    ifAssignedFinalUser: { type: Boolean, default: false },
    ifAssignedMassively: { type: Boolean, default: false },
    status: { type: String},
    userType: { type: String, required: true },
    dateCreated: { type: Date, default: new Date() },
    dateUpdated: { type: Date, default: new Date() },
    //idUserAssignPoint: { type: Schema.Types.ObjectId, ref: 'Distributor' },

});

module.exports = mongoose.model('Assignment', AssignmentSchema);