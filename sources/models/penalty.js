'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {Date} dateCreated
 * @param {ObjectId} distributor
 * @param {Number} redemptionPoints
 */
const PenaltySchema = Schema({
    dateCreated: { type: Date, default: new Date() },
    distributor: { type: Schema.Types.ObjectId, ref: 'Distributor', default: null },
    penaltyPoints: { type: Number, default: 0},
});

module.exports = mongoose.model('Penalty', PenaltySchema);