'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {Date} dateCreated
 * @param {ObjectId} secondaryWholesaler
 * @param {Date} secondaryWholesalerDate
 * @param {ObjectId} distributor
 * @param {Date} distributorDate
 * @param {ObjectId} seller
 * @param {Date} sellerDate
 * @param {Boolean} finalClient
 * @param {Date} finalClientDate
 * @param {String} DN
 * @param {String} ICCStatus
 */
const ICCAssignmentSchema = Schema({
    ICC: { type: String, default: '' },
    dateCreated: { type: Date, default: new Date() },
    secondaryWholesaler: { type: Schema.Types.ObjectId, ref: 'SecondaryWholeSaler', default: null },
    secondaryWholesalerDate: { type: Date, default: null },
    distributor: { type: Schema.Types.ObjectId, ref: 'Distributor', default: null },
    distributorDate: { type: Date, default: null },
    seller: { type: Schema.Types.ObjectId, ref: 'sellerUser', default: null },
    sellerDate: { type: Date, default: null },
    finalClient: { type: Boolean, default: false },
    finalClientDate: { type: Date, default: null },
    DN: { type: String, default: '' }, //telefono
    ICCStatus: { type: String, default: 'Inactivo' },
});

module.exports = mongoose.model('ICCAssignment', ICCAssignmentSchema);