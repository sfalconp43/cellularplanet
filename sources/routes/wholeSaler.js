'use strict'

const express = require('express');
const router = express.Router();
const WholeSalerController = require('../controllers/wholeSaler');


router.get('/admin-user-test', WholeSalerController.test);
router.post('/save-wholesaler-user', WholeSalerController.saveWholeSaler);
router.post('/save-wholesaler-user-web', WholeSalerController.saveWholeSalerWeb);
router.post('/update-wholesaler-user', WholeSalerController.updateWholeSaler);
router.post('/update-wholesaler-user-web', WholeSalerController.updateWholeSalerWeb);
router.post('/update-distributor-user', WholeSalerController.updateUserDistributor);
router.post('/update-distributor-web', WholeSalerController.updateUserDistributorWeb);
router.post('/update-distributor-web-two', WholeSalerController.updateUserDistributorWebTwo);
router.get('/get-wholesaler-user', WholeSalerController.getWholeSalerUser);
router.get('/get-wholesaler-users', WholeSalerController.getWholeSalerUsers);
router.post('/delete-wholesaler-user', WholeSalerController.deleteWholeSalerUser);
router.post('/delete-wholesaler-user-web', WholeSalerController.deleteWholeSalerUserWeb);
router.post('/save-saler-distributor-user', WholeSalerController.saveDistributor)
router.post('/save-distributor-web', WholeSalerController.saveDistributorWeb);
router.post('/save-distributor-web-two', WholeSalerController.saveDistributorWebTwo);
router.get('/get-distributor-user', WholeSalerController.getDistributorUser);
router.get('/get-distributor-users', WholeSalerController.getDistributorsUsers);
router.post('/delete-distributor-user', WholeSalerController.deleteDistributorUser);
router.post('/delete-distributor-web', WholeSalerController.deleteDistributorUserWeb);
router.post('/delete-distributor-web-two', WholeSalerController.deleteDistributorUserWebTwo);




module.exports = router;