'use strict'

const express = require('express');
const router = express.Router();
const SecondarySalerController = require('../controllers/secondaryWholeSaler');

router.get('/admin-user-test', SecondarySalerController.test);
router.post('/save-secondary-saler-user', SecondarySalerController.saveSecondaryWholeSaler);
router.post('/update-secondary-saler-user', SecondarySalerController.updateSecondaryWholeSaler);
router.get('/get-secondary-saler-user', SecondarySalerController.getSecondaryWholeSalerUser);
router.get('/get-secondary-saler-users', SecondarySalerController.getsecondaryWholeSalerUsers);
router.post('/delete-secondary-saler-user', SecondarySalerController.deleteSecondaryWholeSalerUser);





module.exports = router;