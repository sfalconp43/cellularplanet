'use strict'

const express = require('express');
const router = express.Router();
const BuyController = require('../controllers/buy');

router.get('/admin-user-test', BuyController.test);
router.post('/save-buy-user-web', BuyController.saveBuyWeb);
router.post('/update-buy-user', BuyController.updateBuy);
router.get('/get-buy-user', BuyController.getBuyUser);
router.get('/get-buy-users', BuyController.getBuyUsers);
router.post('/delete-buy-user', BuyController.deleteBuyUser);
// router.post('/swap-buy-user', BuyController.makeSwap);
// router.post('/points-buy-user', BuyController.assignPoints);
// router.post('/save-buy-user', BuyController.saveBuy);



module.exports = router;