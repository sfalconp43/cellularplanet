'use strict'

const express = require('express');
const router = express.Router();
const ICCAssignmentController = require('../controllers/ICCAssignment');

router.get('/icc-assignment-test', ICCAssignmentController.test);
router.post('/assign-secondary-wholesaler', ICCAssignmentController.assignSecondaryWholesaler);
router.post('/assign-secondary-wholesaler-web', ICCAssignmentController.assignSecondaryWholesalerWeb);

router.post('/assign-secondary-wholesaler-multi', ICCAssignmentController.assignSecondaryWholesalerMulti);
router.post('/assign-wholesaler-secondary-multi', ICCAssignmentController.assignSecondaryWholesalerMultiMPDist);
router.post('/assign-secondary-wholesaler-dist-multi', ICCAssignmentController.assignSecondaryWholesalerMultiMSDist);
router.post('/assign-distributor-seller-multi', ICCAssignmentController.assignSecondaryWholesalerMultiDistSell);
router.post('/assign-seller-final-user-multi', ICCAssignmentController.assignSellerFinalUser);

router.post('/assign-distributor-principal', ICCAssignmentController.assignDistributorPrincipal);
router.post('/assign-distributor-principal-web', ICCAssignmentController.assignDistributorPrincipalWeb);
router.post('/assign-distributor-secundary', ICCAssignmentController.assignDistributorSecundary);
router.post('/assign-distributor-secundary-web', ICCAssignmentController.assignDistributorSecundaryWeb);
router.post('/assign-seller', ICCAssignmentController.assignSeller);
router.post('/assign-seller-web', ICCAssignmentController.assignSellerWeb);
router.post('/assign-final-client', ICCAssignmentController.assignFinalClient);
router.post('/assign-final-client-web', ICCAssignmentController.assignFinalClientWeb);
router.get('/get-icc-assignment', ICCAssignmentController.getICCAssignment);
router.get('/get-icc-assignment-web', ICCAssignmentController.getICCAssignmentWeb);
router.get('/get-icc-assignments', ICCAssignmentController.getICCAssignments);
router.get('/get-icc-assignments-web', ICCAssignmentController.getICCAssignmentsWeb);

module.exports = router;