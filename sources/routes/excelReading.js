"use strict";

const express = require("express");
const router = express.Router();
const multipart = require("connect-multiparty");
const excelController = require("../controllers/excelReading");
const multipartMiddleware = multipart({ uploadDir: "./public/uploads" });
const fs = require("fs");

//router.post('/read-excel',excelController.readActive ); //ruta normalita que funciona
router.post("/read-excel", multipartMiddleware, excelController.readActive); //prueba para integrar upload
router.post("/read-excel-web", multipartMiddleware, excelController.readActiveWeb); //prueba para integrar upload


// var multiparty = require("connect-multiparty"),
//   multipartyMiddleware = multiparty({ uploadDir: "./public/uploads" });

// router.post("/upload-file", multipartyMiddleware, function(req, res) {
//   console.log(req.body, req.files);

//   var file = req.files.file;

//   var path = file.path;

//   var newName = file.name;

//   var fileSplit = path.split("/");
//   var fileName = fileSplit[2];

//   //console.log(fileName);
//   //console.log(newName);
//   ////console.log(path);
//   ////console.log('esto es contenido de file', file);
//   console.log("este es el nombre", file.name);
//   fs.rename(
//     `./public/uploads/${fileName}`,
//     `./public/uploads/${newName}`,
//     err => {
//       if (err) {
//         console.error(err);
//         return;
//       }
//       //done
//     }
//   );

//   res.status(200).send("OK");
// });

module.exports = router;
