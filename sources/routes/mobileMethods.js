'use strict'

const express = require('express');
const router = express.Router();
const MobileMethodsController = require('../controllers/mobileMethods');

router.get('/mobile-test', MobileMethodsController.test);
router.get('/get-mp-users', MobileMethodsController.getUsersMP);
router.get('/get-ms-users', MobileMethodsController.getUsersMS);
router.get('/get-d-users', MobileMethodsController.getUsersD);
router.get('/get-distributors', MobileMethodsController.getDistributors);
router.get('/get-points-mobile', MobileMethodsController.getPoints);
// router.post('/create-penalty', PenaltyController.createPenalty);

module.exports = router;