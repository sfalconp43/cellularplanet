'use strict'

const express = require('express');
const router = express.Router();

var correoSend = require('../controllers/mail');
router.post('/email2', correoSend.sendMail);
router.post('/forgot-password', correoSend.forgotPassword);
router.post('/change-password/change-password-final', correoSend.changePassword);



module.exports = router;