'use strict'

const express = require('express');
const router = express.Router();
const DistributorController = require('../controllers/distributor');

router.get('/admin-user-test', DistributorController.test);
router.post('/save-seller-user', DistributorController.saveSellerUser);
router.post('/save-seller-web', DistributorController.saveSellerUserWeb);
router.post('/update-seller-user', DistributorController.updateSeller);
router.post('/update-seller-web', DistributorController.updateSellerWeb);
router.get('/get-seller-user', DistributorController.getSellerUser);
router.get('/get-seller-users', DistributorController.getSellerUsers);
router.post('/delete-seller-user', DistributorController.deleteSellerUser);
router.post('/delete-seller-web', DistributorController.deleteSellerUserWeb);



module.exports = router;