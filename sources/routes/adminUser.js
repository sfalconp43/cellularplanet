'use strict'

const express = require('express');
const router = express.Router();
const AdminUserController = require('../controllers/adminUser');

router.get('/admin-user-test', AdminUserController.test);
router.post('/save-admin-user', AdminUserController.saveAdminUser);
router.get('/get-admin-user', AdminUserController.getAdminUser);
router.get('/get-admin-users', AdminUserController.getAdminUsers);
router.post('/update-admin-user', AdminUserController.updateAdminUser);
router.post('/delete-admin-user', AdminUserController.deleteAdminUser);

// router.get('/tablero-administrador', (request, response) => {
//     return response.status(200).render('tablero-administrador');
// });

module.exports = router;