'use strict'

const express = require('express');
const router = express.Router();
const RedemptionController = require('../controllers/redemption');

router.get('/redemption-test', RedemptionController.test);
router.post('/create-redemption', RedemptionController.createRedemption);
router.post('/accept-redemption', RedemptionController.acceptRedemption);
router.post('/cancel-redemption', RedemptionController.cancelRedemption);
router.get('/get-redemptions', RedemptionController.getRedemptions);
router.post('/get-points', RedemptionController.getPoints);

module.exports = router;