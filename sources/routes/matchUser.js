'use strict'

const express = require('express');
const router = express.Router();
const matchController = require('../controllers/validateUser');

router.post('/match-user', matchController.match);

module.exports = router;