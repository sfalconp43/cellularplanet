'use strict'

const express = require('express');
const router = express.Router();


var EmailCtrl = require('../controllers/mailCtrl');
// var EmailCtrl2 = require('../controllers/mail');

//email route

router.post('/email', EmailCtrl.sendEmail);

// router.post('/email2', EmailCtrl2.mailSender);


module.exports = router;