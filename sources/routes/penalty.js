'use strict'

const express = require('express');
const router = express.Router();
const PenaltyController = require('../controllers/penalty');

router.get('/penalty-test', PenaltyController.test);
router.post('/create-penalty', PenaltyController.createPenalty);

module.exports = router;