'use strict'

const express = require('express');
const router = express.Router();
const LoginController = require('../controllers/login');

router.get('/login-test', LoginController.test);
router.post('/login-app', LoginController.login);
router.get('/logout', LoginController.logout);
// router.post('/assign-distributor-principal', ICCAssignmentController.assignDistributorPrincipal);
// router.post('/assign-distributor-secundary', ICCAssignmentController.assignDistributorSecundary);
// router.post('/assign-seller', ICCAssignmentController.assignSeller);
// router.post('/assign-final-client', ICCAssignmentController.assignFinalClient);
// router.get('/get-icc-assignment', ICCAssignmentController.getICCAssignment);
// router.get('/get-icc-assignments', ICCAssignmentController.getICCAssignments);

module.exports = router;