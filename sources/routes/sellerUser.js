'use strict'

const express = require('express');
const router = express.Router();
const sellerUserController = require('../controllers/sellerUser');

router.get('/admin-user-test', sellerUserController.test);
router.post('/save-seller-user', sellerUserController.saveSellerUser)

module.exports = router;