'use strict'

const express = require('express');
const router = express.Router();
const AssignmentController = require('../controllers/assignment');

router.post('/save-assignment', AssignmentController.saveAssignment);
router.post('/save-assignment-web', AssignmentController.saveAssignmentWeb);
router.post('/update-assignment-user', AssignmentController.updateAssignment);
router.post('/update-assignment-user-web', AssignmentController.updateAssignmentWeb);
router.get('/get-assignment-user', AssignmentController.getAssignment);
router.get('/get-assignment-users', AssignmentController.getAssignments);
router.get('/get-user-list', AssignmentController.getUsersById2);
router.post('/delete-assignment-user', AssignmentController.deleteAssignmentUser);
router.post('/delete-assignment-user-web', AssignmentController.deleteAssignmentUserWeb);
// router.post('/count-points', AssignmentController.countPoints);
router.post('/distributor-points', AssignmentController.pointsDistributor);
router.post('/distributor-points-web', AssignmentController.pointsDistributorWeb);



module.exports = router;