'use strict'

const express = require('express');
const router = express.Router();
const StatisticsController = require('../controllers/statistics');

router.get('/statistics-test', StatisticsController.test);
router.get('/get-statistics', StatisticsController.getStatistics);
router.get('/get-statistics-dashboard', StatisticsController.getStatisticsDashboard);
router.get('/get-lasts', StatisticsController.getLasts);

module.exports = router;