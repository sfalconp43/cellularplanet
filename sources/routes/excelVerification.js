"use strict";

const express = require("express");
const router = express.Router();
const multipart = require("connect-multiparty");
const ExcelVerificationController = require("../controllers/excelVerification");

const multipartMiddleware = multipart({ uploadDir: "./public/uploads" });

router.post('/excel-test', ExcelVerificationController.test);
router.post("/excel-verification-activos", multipartMiddleware, ExcelVerificationController.activosVerification);
router.post("/excel-verification-portados", multipartMiddleware, ExcelVerificationController.portadosVerification);
// router.post("/excel-verification-portados", multipartMiddleware, ExcelVerificationController.portadosVerification);



module.exports = router;
