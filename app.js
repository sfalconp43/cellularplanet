'use strict'

var express = require('express');
var bodyParser = require('body-parser');
const hbs = require('hbs');

const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');

var app = express();

const WholeSaler = require('./sources/models/wholeSaler');
const Distributor = require('./sources/models/distributor');
const sellerUser = require('./sources/models/sellerUser');
const assignment = require('./sources/models/assignment');
const secondaryWholeSaler = require('./sources/models/secondaryWholeSaler');
const ICCAssignment = require('./sources/models/ICCAssignment');
const redemption = require('./sources/models/redemption');

const loginMiddleware = require('./sources/middlewares/authentication');




// configuracion HBS
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');

// Configuracion del inicio de sesion y mensajes flash
app.use(session({
    secret: 'Que Equipo? ¡LINCES!',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// CORS

// public
app.use(express.static(__dirname + '/public'));

// Cargar archivos de rutas
const adminUserRoutes = require('./sources/routes/adminUser');
app.use('/', adminUserRoutes);

// Cargar archivos de rutas de Matoristas
const wholeSalerRoutes = require('./sources/routes/wholeSaler');
app.use('/', wholeSalerRoutes);

// Cargar archivos de rutas de Distribuidor
const DistributorRoutes = require('./sources/routes/distributor');
app.use('/', DistributorRoutes);

// Cargar archivos de rutas de Vendedor
const sellerUserRoutes = require('./sources/routes/sellerUser');
app.use('/', sellerUserRoutes);

// Cargar archivos de rutas de Myorista Secundario
const secondaryWholeSalerRoutes = require('./sources/routes/secondaryWholeSaler');
app.use('/', secondaryWholeSalerRoutes);

// Cargar archivos de rutas de Asignación
const assignmentUserRoutes = require('./sources/routes/assignment');
app.use('/', assignmentUserRoutes);

// Cargar archivos de rutas de excel
const excelRoutes = require('./sources/routes/excelReading');
app.use('/', excelRoutes);

// Cargar archivos de rutas de Asignación
const matchRoutes = require('./sources/routes/matchUser');
app.use('/', matchRoutes);

// Cargar archivos de rutas de Compra
const buyRoutes = require('./sources/routes/buy');
app.use('/', buyRoutes);


// Cargar archivos de rutas de Email
const emailRoutes = require('./sources/routes/email');
app.use('/', emailRoutes);


//email otra ruta
const correoRoutes = require('./sources/routes/email2');
app.use('/', correoRoutes);

// JONATHAN
// Cargar archivos de rutas de nueva asignacion
const ICCAssignmentRoutes = require('./sources/routes/ICCAssignment');
app.use('/', ICCAssignmentRoutes);

// Cargar archivos de rutas de nueva asignacion
const redemptionRoutes = require('./sources/routes/redemption');
app.use('/', redemptionRoutes);

// Cargar archivos de rutas de nueva asignacion
const penaltyRoutes = require('./sources/routes/penalty');
app.use('/', penaltyRoutes);

// Cargar archivos de rutas de nueva asignacion
const loginRoutes = require('./sources/routes/login');
app.use('/', loginRoutes);

const statisticsRoutes = require('./sources/routes/statistics');
app.use('/', statisticsRoutes);

const mobileMethodsRoutes = require('./sources/routes/mobileMethods');
app.use('/', mobileMethodsRoutes);

const excelVerificationRoutes = require('./sources/routes/excelVerification');
app.use('/', excelVerificationRoutes);

app.get('/dashboard', (request, response) => {
    return response.status(200).render('dashboard');
});

app.get('/dashboard', (request, response) => {
    return response.status(200).render('dashboard');
});

app.get('/tablero-mayorista-principal', loginMiddleware.verifyAuthentication, async (request, response) => {
    const assignmentsMPAll = await ICCAssignment.find();
    let inactivosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
    let activosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Activo');
    let portadosMPAll = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Portado');
    let inactivosMPAllP = 0;
    let activosMPAllP = 0;
    let portadosMPAllP = 0;

    if (assignmentsMPAll.length > 0) {
        if (inactivosMPAll.length > 0) {
            inactivosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMPAll.length;
        }

        if (activosMPAll.length > 0) {
            activosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMPAll.length;
        }

        if (portadosMPAll.length > 0) {
            portadosMPAllP = assignmentsMPAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMPAll.length;
        }
    }

    return response.status(200).render('tablero-mayorista-principal', {
        inactivosMPAllP: inactivosMPAllP * 100,
        activosMPAllP: activosMPAllP * 100,
        portadosMPAllP: portadosMPAllP * 100,
        inactivosMPAll: inactivosMPAll.length,
        activosMPAll: activosMPAll.length,
        portadosMPAll: portadosMPAll.length,
        total: inactivosMPAll.length + activosMPAll.length + portadosMPAll.length
    });
});

app.get('/tablero-mayorista-principal-2', (request, response) => { //back up vista de admin ltd
    return response.status(200).render('tablero-mayorista-principal-2');
});

app.get('/tablero-mayorista-secundario', loginMiddleware.verifyAuthentication, async (request, response) => {

    const assignmentsMSAll = await ICCAssignment.find({ secondaryWholesaler: request.user_id });
    let inactivosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
    let activosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Activo');
    let portadosMSAll = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Portado');
    let inactivosMSAllP = 0;
    let activosMSAllP = 0;
    let portadosMSAllP = 0;

    if (assignmentsMSAll.length > 0) {
        if (inactivosMSAll.length > 0) {
            inactivosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsMSAll.length;
        }

        if (activosMSAll.length > 0) {
            activosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsMSAll.length;
        }

        if (portadosMSAll.length > 0) {
            portadosMSAllP = assignmentsMSAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsMSAll.length;
        }
    }

    return response.status(200).render('tablero-mayorista-secundario',{
        inactivosMSAllP: inactivosMSAllP * 100,
        activosMSAllP: activosMSAllP * 100,
        portadosMSAllP: portadosMSAllP * 100,
        inactivosMSAll: inactivosMSAll.length,
        activosMSAll: activosMSAll.length,
        portadosMSAll: portadosMSAll.length,
        total: inactivosMSAll.length + activosMSAll.length + portadosMSAll.length

    });


    // return response.status(200).render('tablero-mayorista-secundario');
});

//prueba de referencia
app.get('/tablero-mayorista-secundario-back', (request, response) => {
    return response.status(200).render('tablero-mayorista-secundario-back');
});


app.get('/tablero-distribuidor', loginMiddleware.verifyAuthentication, async (request, response) => {
    const assignmentsDAll = await ICCAssignment.find({ distributor: request.user_id});
    let inactivosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
    let activosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Activo');
    let portadosDAll = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Portado');
    let inactivosDAllP = 0;
    let activosDAllP = 0;
    let portadosDAllP = 0;
    // console.log('Todo');
    // console.log(assignmentsDAll);

    if (assignmentsDAll.length > 0) {
        if (inactivosDAll.length > 0) {
            inactivosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsDAll.length;
        }

        if (assignmentsDAll.length > 0) {
            activosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsDAll.length;
        }

        if (assignmentsDAll.length > 0) {
            portadosDAllP = assignmentsDAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsDAll.length;
        }
    }

    return response.status(200).render('tablero-distribuidor',{
        inactivosDAllP: inactivosDAllP * 100,
        activosDAllP: activosDAllP * 100,
        portadosDAllP: portadosDAllP * 100,
        inactivosDAll: inactivosDAll.length,
        activosDAll: activosDAll.length,
        portadosDAll: portadosDAll.length,
        total: inactivosDAll.length + activosDAll.length + portadosDAll.length

    });

    
    // return response.status(200).render('tablero-distribuidor');
});

//referencia de bd
app.get('/tablero-distribuidor2', (request, response) => {
    return response.status(200).render('tablero-distribuidor2');
});

app.get('/tablero-vendedor', loginMiddleware.verifyAuthentication, async (request, response) => {
    const assignmentsVAll = await ICCAssignment.find({ seller: request.user_id });
                    let inactivosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Inactivo');
                    let activosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Activo');
                    let portadosVAll = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Portado');
                    let inactivosVAllP = 0;
                    let activosVAllP = 0;
                    let portadosVAllP = 0;
                    console.log('Todo');
                    console.log(assignmentsVAll);

                    if (assignmentsVAll.length > 0) {
                        if (inactivosVAll.length > 0) {
                            inactivosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Inactivo').length / assignmentsVAll.length;
                        }

                        if (assignmentsVAll.length > 0) {
                            activosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Activo').length / assignmentsVAll.length;
                        }

                        if (assignmentsVAll.length > 0) {
                            portadosVAllP = assignmentsVAll.filter((assignment) => assignment.ICCStatus === 'Portado').length / assignmentsVAll.length;
                        }
                    }

                    return response.status(200).render( 'tablero-vendedor',{
                        status: true,
                        message: 'statistics',
                        inactivosVAllP: inactivosVAllP * 100,
                        activosVAllP: activosVAllP * 100,
                        portadosVAllP: portadosVAllP * 100,
                        inactivosVAll: inactivosVAll.length,
                        activosVAll: activosVAll.length,
                        portadosVAll: portadosVAll.length,
                        total: inactivosVAll.length + activosVAll.length + portadosVAll.length

                    });

    // return response.status(200).render('tablero-vendedor');
});

app.get('/usuarios-mayoristas-secundarios', loginMiddleware.verifyAuthentication, (request, response) => {

    secondaryWholeSaler.find().then( (mayoristasSecundarios) => {
        return response.status(200).render('usuarios-mayoristas-secundarios',{
            user: request.user,
            messageError: request.flash('error-message'),
            messageSuccess: request.flash('success-message'),
            mayoristasSecundarios: mayoristasSecundarios
        });
    }).catch( (error) => {
        console.log(error);
    });
});

//de prueba
app.get('/usuarios-mayoristas-secundarios-back', (request, response) => {

    WholeSaler.find().then( (mayoristasSecundarios) => {
        return response.status(200).render('usuarios-mayoristas-secundarios-back',{
            user: request.user,
            messageError: request.flash('error-message'),
            messageSuccess: request.flash('success-message'),
            mayoristasSecundarios: mayoristasSecundarios
        });
    }).catch( (error) => {
        console.log(error);
    });
});


app.get('/usuarios-distribuidor-por-mayorista-principal', loginMiddleware.verifyAuthentication, (request, response) => {
    Distributor.find().then( (distribuidores) => {
        console.log(distribuidores);
        return response.status(200).render('usuarios-distribuidores-por-mayorista-principal',{
            user: request.user,
            messageError: request.flash('error-message'),
            messageSuccess: request.flash('success-message'),
            distribuidores: distribuidores
        });
    }).catch( (error) => {
        console.log(error);
    });
});


// app.get('/usuarios-distribuidor-por-mayorista-principal-back', (request, response) => {
//     Distributor.find().then( (distribuidores) => {
//         console.log(distribuidores);
//         return response.status(200).render('usuarios-distribuidores-por-mayorista-principal-back',{
//             user: request.user,
//             messageError: request.flash('error-message'),
//             messageSuccess: request.flash('success-message'),
//             distribuidores: distribuidores
//         });
//     }).catch( (error) => {
//         console.log(error);
//     });
// });


app.get('/usuarios-distribuidor-por-mayorista-secundario', loginMiddleware.verifyAuthentication, (request, response) => {
    Distributor.find({ idOfCreator: request.user._id }).then( (distribuidores) => {
        console.log(distribuidores);
        return response.status(200).render('usuarios-distribuidores-por-mayorista-secundario',{
            user: request.user,
            messageError: request.flash('error-message'),
            messageSuccess: request.flash('success-message'),
            distribuidores: distribuidores
        });
    }).catch( (error) => {
        console.log(error);
    });
});

// //por prueba de referencia
// app.get('/usuarios-distribuidor-por-mayorista-secundario-back', (request, response) => {
//     Distributor.find({ idOfCreator: request.user._id }).then( (distribuidores) => {
//         console.log(distribuidores);
//         return response.status(200).render('usuarios-distribuidores-por-mayorista-secundario-back',{
//             user: request.user,
//             messageError: request.flash('error-message'),
//             messageSuccess: request.flash('success-message'),
//             distribuidores: distribuidores
//         });
//     }).catch( (error) => {
//         console.log(error);
//     });
// });



app.get('/usuarios-vendedores-por-distribuidor', loginMiddleware.verifyAuthentication, (request, response) => {
    console.log(request.user._id)
    sellerUser.find({ idOfCreator: request.user._id }).then( (vendedores) => {
        console.log(vendedores);

    sellerUser.find().then((vendedores2) => {
        console.log(vendedores2);
            return response.status(200).render('usuarios-vendedores-por-distribuidor',{
                user: request.user,
                messageError: request.flash('error-message'),
                messageSuccess: request.flash('success-message'),
                vendedores: vendedores,
                vendedores2: vendedores2
            });
        })
    }).catch( (error) => {
        console.log(error);
    });
});


require('./sources/controllers/passport')(passport);

app.get('/login', (request, response) => {
    response.render('login', {
        messageError: request.flash('error-message'),
        messageSuccess: request.flash('success-message'),
    });
});

app.post('/login', passport.authenticate('local-login', {
    failureRedirect: '/login',
    failureFlash: true
}), (request, response) => {
    if (request.user.ifPrincipal != undefined) {
        if (request.user.ifPrincipal) {
            response.redirect('/tablero-mayorista-principal');
        } else {
            response.redirect('/tablero-mayorista-secundario');
        }
    } else {
        if (request.user.points != undefined) {
            response.redirect('/tablero-distribuidor');
        } else {
            response.redirect('/tablero-vendedor');
        }
    }
});


app.get('/verificacion-por-archivo', loginMiddleware.verifyAuthentication, async (request, response) => {

    ICCAssignment.find().then((iccs)=>{
        console.log(iccs);
        return response.status(200).render('verificacion-por-archivo', {iccs: iccs});

        

    });
});



app.get('/prueba', (request, response) => {
    response.render('prueba', {
    });
});

app.get('/asignacion-a-usuario-sucursal', loginMiddleware.verifyAuthentication, async (request, response) => {
    let iccs = await ICCAssignment.find({distributor: null}).populate("secondaryWholesaler");
    console.log('mayoristas secundarios', iccs);

    ICCAssignment.find().then((asignaciones) => {

    // console.log('asignaciones', asignaciones);

    secondaryWholeSaler.find().then((sucursales2) => {
        // console.log(sucursales2);
            return response.status(200).render('asignacion-a-usuario-sucursal',{
                user: request.user,
                messageError: request.flash('error-message'),
                messageSuccess: request.flash('success-message'),
                iccs: iccs,
                sucursales2: sucursales2
            });
        })
    }).catch( (error) => {
        console.log(error);
    });
});

app.get('/asignacion-a-usuario-distribuidor-mp', loginMiddleware.verifyAuthentication, async (request, response) => {
    let iccs = await ICCAssignment.find({secondaryWholesaler: null}).populate("distributor")
    console.log('Distribuidores', iccs);


    ICCAssignment.find().then( (distribuidores2) => {
        // console.log(distribuidores2);

    Distributor.find().then((distribuidores3) => {
        // console.log('distribuidores', distribuidores3);
            return response.status(200).render('asignacion-a-usuario-distribuidor-mp',{
                user: request.user,
                messageError: request.flash('error-message'),
                messageSuccess: request.flash('success-message'),
                iccs: iccs,
                distribuidores3: distribuidores3
            });
        })
    }).catch( (error) => {
        console.log(error);
    });
});

app.get('/asignacion-a-usuario-distribuidor-ms', loginMiddleware.verifyAuthentication, async (request, response) => {
    let iccs = await ICCAssignment.find({distributor : { $exists: true, $ne: null }, secondaryWholesaler: request.user._id}).populate("distributor");
    
    console.log('LOS ICCS', iccs);
    // console.log('ID DEK USUAARIO', request.user_id);

    ICCAssignment.find().then( (distribuidores2) => {
        // console.log(distribuidores2);

    Distributor.find({idOfCreator: request.user._id }).then((distribuidores3) => {
        console.log('SUS DISTRIBUIDORES', distribuidores3);
            return response.status(200).render('asignacion-a-usuario-distribuidor-ms',{
                user: request.user,
                messageError: request.flash('error-message'),
                messageSuccess: request.flash('success-message'),
                distribuidores3: distribuidores3,
                iccs: iccs
            });
        })
        // return response.status(200).render('asignacion-a-usuario-distribuidor',{
        //     user: request.user,
        //     messageError: request.flash('error-message'),
        //     messageSuccess: request.flash('success-message'),
        //     distribuidores2: distribuidores2
        // });
    }).catch( (error) => {
        console.log(error);
    });
});

app.get('/asignacion-a-usuario-vendedor-por-distribuidor', loginMiddleware.verifyAuthentication, async (request, response) => {

    let iccs = await ICCAssignment.find({distributor: request.user._id, seller : { $exists: true, $ne: null }}).populate("seller");
    console.log('LOS ICCS', iccs);


    sellerUser.find({idOfCreator: request.user._id }).then((vendedores2) => {
        console.log('VENDEDORES', vendedores2);

    // ICCAssignment.find({distributor: request.user._id}).then( (vendedores) => {
        // console.log(vendedores);
            return response.status(200).render('asignacion-a-usuario-vendedor-por-distribuidor',{
                user: request.user,
                messageError: request.flash('error-message'),
                messageSuccess: request.flash('success-message'),
                // vendedores: vendedores,
                vendedores2: vendedores2,
                iccs : iccs
            });
        // })
    }).catch( (error) => {
        console.log(error);
    });
});

app.get('/asignacion-usuario-final-por-vendedor', loginMiddleware.verifyAuthentication, (request, response) => {

    ICCAssignment.find({finalClient : true}).then( (usuariofinal) => {
        console.log('usuarios final', usuariofinal);
        return response.status(200).render('asignacion-usuario-final-por-vendedor',{
            user: request.user,
            messageError: request.flash('error-message'),
            messageSuccess: request.flash('success-message'),
            usuariofinal: usuariofinal,
        });
    }).catch( (error) => {
        console.log(error);
    });
});

app.get('/redencion-de-puntos', loginMiddleware.verifyAuthentication, async (request, response) => {

        Distributor.find().then( (redencion) => {
        // console.log('CONSULTA TODOS LOS DISTR', redencion);
        return response.status(200).render('redencion-de-puntos',{
            user: request.user,
            messageError: request.flash('error-message'),
            messageSuccess: request.flash('success-message'),
            redencion: redencion,
            // iccs: iccs
        });
    }).catch( (error) => {
        console.log(error);
    })
});

app.get('/redencion-de-puntos-distribuidor', loginMiddleware.verifyAuthentication, async (request, response) => {

    let redenciones = await redemption.find({distributor: request.user._id}).populate("distributor")
    console.log('redenciones', redenciones);

    Distributor.find({_id: request.user._id }).then( (redencion) => {
    console.log('DISTRIBUIDOR', redencion);
    return response.status(200).render('redencion-de-puntos-distribuidor',{
        user: request.user,
        messageError: request.flash('error-message'),
        messageSuccess: request.flash('success-message'),
        redenciones: redenciones,
        redencion: redencion
        // iccs: iccs
    });
}).catch( (error) => {
    console.log(error);
})
});

app.get('/forgot-password', (request, response) => {
    response.render('forgot-password', {
        messageError: request.flash('error-message'),
        messageSuccess: request.flash('success-message'),
    });
});


app.get('/change-password/:id', (request, response) => {
    response.render('change-password', {
        messageError: request.flash('error-message'),
        messageSuccess: request.flash('success-message'),
        id: request.params.id
    });
});



// Rutas

// Exportar
module.exports = app;